---
date: Tue, 01 Aug 2023 12:04:17 -0700
title: Node Applications and Server-Rendered Web Components
description: Imagine a world where HTML pages and templates can easily be written using familiar web components concepts.
---

A key aspect of the web components ecosystem I believe will need to be thoroughly solved and proven in real production applications before WCs can be adopted in larger numbers is server rendering for "fullstack" development in popular environments such as Node.

Imagine a world where HTML pages and templates can easily be written using familiar web components concepts and then statically and/or dynamically rendered and sent to the browser. From there, you can once again target *certain* tag names for progressive enhancement as client-side web components. Call that hydration or islands or whatever you like.

Some libraries I'm aware of which allow for this:

* [Lit SSR](https://www.npmjs.com/package/@lit-labs/ssr)
* [FAST SSR](https://www.npmjs.com/package/@microsoft/fast-ssr)
* [Enhance SSR](https://github.com/enhance-dev/enhance-ssr)
* [WebC](https://github.com/11ty/webc)

In all cases, available documentation and real-world examples is scarce. This is by no means an indictment of these projects—I'm merely saying we have a long way yet to go.

I'd love to tinker with a sample starter project and compare using these and other similar WC SSR libraries. [Got any recommendations?](/contact)

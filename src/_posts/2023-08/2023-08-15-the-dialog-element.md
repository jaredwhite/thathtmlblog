---
date: Tue, 15 Aug 2023 11:05:45 -0700
title: HTML's New Dialog
description: Seriously one of the most awesome additions to vanilla HTML in recent memory.
link_url: https://gomakethings.com/an-intro-to-the-dialog-element/
---

The `<dialog>` element is seriously one of the most awesome additions to vanilla HTML in recent memory, and the API it provides through CSS and JavaScript is quite exciting.

Chris Ferdinandi [provides a comprehensive overview](https://gomakethings.com/an-intro-to-the-dialog-element/) of how to use this new element, particularly as a compelling replacement for old-school methods like `prompt`:

> Two years ago, I wrote about how the alert(), prompt(), and confirm() methods were being deprecated. At the time, I lamented that there wasn’t a great replacement for any of them. That’s no longer the case. Today, I wanted to give you a primer on the dialog element. Let’s dig in!

Browser support is [pretty good these days](https://caniuse.com/mdn-html_elements_dialog), but as always double-check it makes sense to use for your demographics or if you might need to reach for [a polyfill](https://github.com/GoogleChrome/dialog-polyfill).

---
date: Mon, 21 Aug 2023 11:17:51 -0700
title: Property-based Class Utilities
description: What if your "utility classes" were just setting CSS Custom Properties?
link_url: https://crinkles.dev/writing/going-back-to-css-only-after-years-of-scss/
---

This whole article by Kevin Pennekamp, [Going back to CSS-only after years of SCSS](https://crinkles.dev/writing/going-back-to-css-only-after-years-of-scss/), is a fabulous read, but I'd like to zero in particularly on a really cool idea I haven't seen explored much elsewhere, and that is what Kevin calls "class utilities".

Not to be confused with utility classes, class utilities only set CSS custom properties (aka CSS variables) which are then utilized by a specific layout or component-type class. In Kevin's example, he has a `.tiles` class which sets up a simple CSS grid layout. Then he has classes which are prefixed with double-dashes to reinforce that they only set a variable, for example `--gap-0`, `--gap-1`, etc. These classes look something like this:

```css
.--gap-1 {
  --layout-gap: var(--token-space-1);
}
```

Because `.tiles` uses the `--layout-gap` variable to set the grid gap, a class utility like this will influence that gap—essentially just shorthanding `class="tiles" style="--layout-gap: var(--token-space-1)"` down to `class="tiles --gap-1"`.

Truth be told, I'm still personally in favor of using attributes over classes whenever possible, and I'm _also_ in favor of using semantic token names. Thus this technique could also be accomplished like so: 

```css
[layout-gap="small"] {
  --layout-gap: var(--token-space-small);
}
```

for use via `class="tiles" layout-gap="small"` or even `<layout-tiles layout-gap="small">` if you want to custom-element it.

But for folks who are used to utility classes already and would appreciate a soft landing onto more modern vanilla CSS ways of doing things, this is a very neat trick.

---
date: Thu, 19 Sep 2024 14:44:14 -0700
title: The selectmenu Element is No More…Long Live select!
description: A more progressive enhancement approach to a full styling solution of this important form control.
link_url: https://developer.chrome.com/blog/rfc-customizable-select
---

For the longest time, I've been hearing about `<selectmenu>` as the future proposed solution to full styling of dropdown options in forms. We're all aware of the difficulties of styling `<select>`—while it's gotten easier to make the initial control appear customized, you still get an OS-native menu popup when you click on it, and those options are simple text only.

Apparently, there's a whole new plan in the works now which does away with the separate markup structure of a `<selectmenu>`, and instead takes a more _progressive enhancement_ approach building on top of the existing `<select>` and `<option>` syntax. And developers are being asked to provide their feedback on this new approach! [Una Kravets writes on the Chrome Developers Blog](https://developer.chrome.com/blog/rfc-customizable-select):

> Styling form controls like the `<select>` element has been reported as a top developer pain point for years, and we've been working on a solution. While this work is complex and has taken a long time to get right, we're getting very close to landing this feature. A customizable version of the select element is officially in Stage 2 in the WHATWG, with strong cross-browser interest and a prototype for you to test out from Chrome Canary 130.

The two-step process for turning a regular control into a supercharged one is first applying just a bit of CSS:

```css
select, ::picker(select) {
  appearance: base-select;
}
```

This effectively "switches off" native OS styling for both the initial control and the options and gives them both a very minimalist appearance. In this way, it reminds me a lot of how `<dialog>` works. From there, the markup enhancements are pretty straightforward:

```html
<select>
  <button>
    <selectedoption></selectedoption>
    <span>
      // Arrow here
    </span>
  </button>
  // Everything else that will go into the ::picker(select) popover
</select>
```

The initial control you see in the form is entirely encapsulated within `<button>`, and then you add your `<option>` tags as usual—only this time you can put HTML in your options! For example:

```html
<option value="france">
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/120px-Flag_of_France.svg.png" alt="" />
  <span>France</span>
</option>
```

And obviously this means you have full styling hooks for all the various aspects of the `<select>`.

I think this makes a ton of sense, and the nice thing is you still get basic functionality in browsers which don't support the new HTML+CSS syntax. That _might_ not be acceptable in all cases for UX reasons, but much of the time, I suspect it'll be Good Enough™.

**What do you think about the new syntax and feature set?** Check it out and [let them know](https://forms.gle/h5i83K85YfmBvtgp9)!

**Update:** Adam Argyle [crushing it once again with a cool demo](https://nerdy.dev/custom-select-with-transitions-boilerplate) of what you can do with all these new goodies! 🤩

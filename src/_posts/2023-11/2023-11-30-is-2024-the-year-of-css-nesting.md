---
date: Thu, 30 Nov 2023 09:38:45 -0800
title: Is 2024 the Year of CSS Nesting?
description: Browsers stats are starting to look pretty good.
link_url: https://ishadeed.com/article/css-nesting/
---

At long last, nested selectors in CSS are no longer the sole domain of preprocessors like Sass. We can use CSS nesting in all evergreen browsers. This is a huge leap forward for the web platform.

For an excellent overview of how CSS nesting works, [I refer you to Ahmad Shadeed who writes](https://ishadeed.com/article/css-nesting/):

> There are some valid reasons in my opinion which makes nesting CSS useful:
>
> Easier to read CSS.  
> Grouping style together.  
> Scoping specific styles.  
> Styling HTML elements that don’t have a class or ID.  

And with the recent "relaxed syntax" update to the spec which lets browsers interpret nested selectors without requiring `&` or other special characters, it's even more familiar to everyone who's written nested CSS in the past via build tools.

But the question remains: **when will this be safe to use in production**?

Depending on your target audience, it might already be safe. If you're building internal business applications where you know browser support is up-to-date, go for it. If you're providing online services to a technical audience, maybe go for it. Otherwise I would wait a bit longer before shipping your nested selectors directly over the wire.

However, I expect this calculus to change in 2024. [According to Can I Use](https://caniuse.com/css-nesting), the current browser support percentage sits at 80%. I forsee this getting to 85% before too long. If we look at another CSS feature which shipped a bit earlier, [color-mix()](https://caniuse.com/mdn-css_types_color_color-mix), it's at just about 85% now. It wouldn't be a stretch to assume that part-way into 2024, we'll observe a pretty solid rollout of nesting—the stricter syntax at least. (I'd wait until at least 2025 to go full-tilt boogie with the relaxed syntax.)

As always YMMV with this stuff, but my plan in 2024 is to see just how far I can push 100% vanilla CSS (even bidding PostCSS farewell), and **the rollout of nesting is a huge factor**. I also think 2024 may finally be the year I embrace [Cascade Layers](https://caniuse.com/css-cascade-layers), but that's a conversation for another day!

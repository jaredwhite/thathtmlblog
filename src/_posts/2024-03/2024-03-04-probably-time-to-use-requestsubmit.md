---
date: Mon, 04 Mar 2024 13:49:12 -0800
title: It’s Probably Time to Embrace requestSubmit()
description: No more querying or creating submit buttons just to trigger form submissions correctly.
link_url: "https://www.raymondcamden.com/2024/03/01/til-submit-versus-requestsubmit"
---

For a long long time, JavaScript developers would need to query for a submit button inside of a form—or create one if need be!—and then call `click()` on the button in order to programmatically submit a form with the same lifecycle that a user submission would get—validation, triggering a `submit` event, etc. Simply calling `submit()` on a form directly wouldn't give you that…it would merely submit the form immediately, bypassing validation and events.

To solve this, a new method was added to the Form API: `requestSubmit()`. Calling that would be the programmatic equivalent of a user clicking a submit button or hitting their Enter key. However, it took a while to roll out across browsers, with Chrome gaining support in 2019, Firefox in 2020, and Safari in 2022.

At this point, Can I Use reports 92.72% global support for `requestSubmit()` which seems promising for production use without a polyfill. Should libraries which simulate form submissions take steps to ditch the old query/create button trick and go with `requestSubmit()`? Probably.

Raymond Camden [has written about the difference](https://www.raymondcamden.com/2024/03/01/til-submit-versus-requestsubmit) between `submit()` and `requestSubmit()` and shows off a CodePen illustrating those differences, so you should definitely check that out as well. 

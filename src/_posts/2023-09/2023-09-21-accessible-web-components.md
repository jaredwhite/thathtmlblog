---
date: Thu, 21 Sep 2023 14:44:42 -0700
title: Accessible Web Components
description: The definitive speedy guide for learning about accessibility and web components together.
link_url: https://www.matuzo.at/blog/2023/web-components-accessibility-faq/
---

Manuel Matuzović once again [hits it out of the park](https://www.matuzo.at/blog/2023/web-components-accessibility-faq/) with a comprehensive yet concise guide to creating and using accessible web components (and some of the possible pitfalls, particularly when talking about "cross-root" ARIA / shadow DOM). As Manuel explains:

> I specialize in HTML and CSS, but I also write JS. Especially in the last year or so, I wrote quite a lot of JavaScript because we decided to port the front end of one of my clients to web components.
> 
> When I first learned about web components, I had a lot of questions, especially regarding accessibility. While I found answers to many of them, I didn’t know everything I would’ve wanted to know. I wish I had a catalog of all the essential questions and answers when I started. That’s why I decided to design this post in a Q&A format. I’ll ask a question regarding the accessibility of web components, and then I’ll answer it.

Not only is this FAQ now a definitive resource on the topic IMO, but [Manuel just gave a whole talk and presentation](https://www.youtube.com/watch?v=ko1WlkT4Phc) based on the FAQ which is excellent to watch.

Possibly my favorite point Manuel makes is this: "Can web components help improve accessibility? **Yes!**"

If that comes as a surprise to you, time to read through the FAQ and the talk… 😄

---
date: Tue, 08 Aug 2023 13:51:33 -0700
title: React Is Not Modern Web Development
description: Sometimes legacy code is time-honored code. But sometimes…it's not.
link_url: https://joshcollinsworth.com/blog/antiquated-react
---

Like with [yesterday's link](/2023/08/tailwind-death-of-craftsmanship/), we're here to evaluate if a popular piece of technology used by millions of web developers is actually modern new hotness or simply appears to be to the untrained eye. And if it IS in fact a _legacy_ project born out of needing to solve particular problems in a past era, then perhaps it's less hardened & practical and more simply, well, old & busted.

John Collinsworth buries the lede a bit in his [excellent overview of this topic](https://joshcollinsworth.com/blog/antiquated-react), so I'll take the liberty of drawing it out and placing it front and center:

> I don’t think most people using React on a regular basis realize quite how much it’s fallen behind.

Yes. Yes. _A thousand times yes._

Also:

> React hooks are actually kind of outdated.

I'm deleting the rant I just went on a moment ago because I loathe React hooks with a passion fueled by the fire of a thousand burning suns. But let us move on…

> I think it’s important to remember that React was created by Facebook, in order to solve Facebook’s unique set of problems.

**Bingo.**

Facebook's problems aren't your problems, unless you too own a massive social network and it's the year 2013.

And finally:

> You might not realize styling is a solved problem in several other frameworks.

I think perhaps there's a reason both React and [Tailwind](/2023/08/tailwind-death-of-craftsmanship/) often come up in conversation at the same time. React's built-in support for handling anything to do with styling is virtually non-existent—with the `className` and `style`-as-object blunders of React's JSX highlighting its bizarre relationship with standard CSS. And so people have come up with a million solutions, each crappier than the next.

Guess what folks? **YAGNI**. Just pick a better framework—or none at all—and [write some CSS](https://justaspec.buzzsprout.com/1863126/12996379-i-just-wanna-write-css).

And honestly I could say that about any number of other things. For example, tons of frontend frameworks are [recalibrating around the concept of Signals](https://dev.to/this-is-learning/the-evolution-of-signals-in-javascript-8ob) as a way to enable fine-grained reactivity in a really straightforward manner. (I personally believe [Preact's standalone Signals library](https://github.com/preactjs/signals) is one of the best libraries in recent memory and one I have started liberally using.)

React? Nowhere to be seen, except for a vague promise of future compiler optimizations.

And don't even get me started on the trainwreck that is React Server Components (RSCs). I even wrote a haiku about it!

> VDOM on server  
> seems to send HTML  
> alas, only once

This is despite the fact that the idea of a component which can only run on the server is actually extremely attractive. [Who knew!](https://viewcomponent.org/)

Anyway, Josh's article goes to great lengths to explain all of these issues, and while I might not share his predilection for Svelte over, say, Lit, I can wholeheartedly [recommend the read](https://joshcollinsworth.com/blog/antiquated-react)—especially with this epic closer about what the future may hold:

> What will that new thing be? I don’t know. Maybe it’ll just be the web platform. Maybe we won’t even need frameworks.

_Hear hear._

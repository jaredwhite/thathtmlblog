---
date: 2023-10-24T08:31:00-07:00
link_url: https://www.spicyweb.dev/building-courseware-i-understand/
title: 'I Made a Thing: CSS Nouveau'
description: >-
  It's a course to help you learn how to build 100% vanilla CSS architectures
  that can scale.
---
Is it an ad when it's your own thing? 🤔

Allow me, if you will, to indulge myself for a brief moment and let you know about [the new course I just launched over at The Spicy Web, *CSS Nouveau*](https://www.spicyweb.dev/css-nouveau). It features my latest, up-to-date, pedal-to-the-metal thinking on how to build up a 100% vanilla CSS architecture from scratch, from&nbsp;*design tokens*&nbsp;to&nbsp;*global stylesheets*&nbsp;to&nbsp;*encapsulated components*.

I also [wrote a blog post about it](https://www.spicyweb.dev/building-courseware-i-understand/)—and more broadly, how even the notion of building a course (any course!) in the first place was **quite challenging** for me on a personal level. Call it a personality quirk, a hangup, what have you…which is why I'm able to breathe a&nbsp;*huge*&nbsp;sigh of relief now that it's finally out the door (and future installments can be released in a dramatically simpler fashion now that the platform is already built out). *Whew!*

And now, back to your regularly scheduled **hypertext-flavored news updates**… 🤓
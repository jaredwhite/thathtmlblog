---
date: Thu, 20 Jul 2023 10:09:52 -0700
title: Talking gTLDs on Just a Spec Podcast
description: I'm excited to see this land in browsers soon.
link_url: https://justaspec.buzzsprout.com/1863126/13252632-til-about-gtlds-ftw
---

Ayush and I once again up to our usual shenanigans on the [Just a Spec Podcast, this time talking about DNS and gTLDs](https://justaspec.buzzsprout.com/1863126/13252632-til-about-gtlds-ftw):

> It’s hard to imagine in this current world of any dot-something you could possibly imagine when looking to register a domain name, but there was once a past era of the internet when all we had was .com, .net, and .org. A dark time. A sad time. How did we get there in the first place? And how did we eventually arrive here at today’s promised land? Join us for this rousing episode all about: gTLDs! (and IANA, and ICANN, and IETF, and…)

And I forgot to mention, we share some of our goofier registered domain names and DNS-related pranks over the years, so you won't want to miss this one…

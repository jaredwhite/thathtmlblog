---
date: Mon, 27 May 2024 14:48:04 -0700
title: "Catching Up on Web Specs with Just a Spec"
description: In case you missed it, here’s what we’ve been talking about on the Just a Spec podcast.
link_url: https://justaspec.show
---

It's been a while since I posted here about the **Just a Spec** podcast I've been producing along with my friend Ayush Newatia, so ICYMI, we've talked about a number of notable topics recently:

* [Interop 2024 and the State of the Industry](https://justaspec.buzzsprout.com/1863126/14900683-interop-2024-and-the-state-of-the-industry) – in this episode we run through a very entertaining list of all the goodies being worked on by browser vendors as part of Interop 2024, as well as muse on the idea that perhaps the complexity of the web platform is just too damn high?
* [The Plumbing That Makes the Web Move](https://justaspec.buzzsprout.com/1863126/14984809-the-plumbing-that-makes-the-web-move) – a meaty conversation all about the various levels of specifications and conventions which make the World-Wide Web go: from TCP/IP to HTTP to WebSockets and beyond.
* [This State of Affairs](https://justaspec.buzzsprout.com/1863126/15074787-this-state-of-affairs) – where data lives, how to retrieve data, how to change data, how to track updates to data and provide feedback accordingly to the user…in other words, state.

Later this week we'll be releasing our next episode covering the **State of HTML 2023** survey results which are finally here, so if you don't want to miss it, [be sure to subscribe to the podcast](https://justaspec.show).

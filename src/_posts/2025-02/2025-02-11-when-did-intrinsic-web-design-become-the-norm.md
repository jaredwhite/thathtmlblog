---
date: Tue, 11 Feb 2025 17:50:33 -0800
title: Hey, When Did Intrinsic Web Design Become the Norm?
description: By combining the best of the media query world with the new fluid world, we can build ambitious and eye-catching layouts and content presentations.
link_url: https://www.smashingmagazine.com/2025/02/taking-rwd-to-the-extreme/
---

I'll admit it: I _rarely_ think about the mechanisms of "old school" **Responsive Web Design** (RWD) any more. In fact, I'm usually surprised—and some measure of annoyed—any time I actually reach for a media query, because somehow it feels like a gnarly hack rather than a good idea.

In essence, I've grown so accustomed to the tenets of **Intrinsic Web Design** (IWD) as an evolution of RWD, it's just what comes naturally to me now. Creating a CSS Grid where the min/max cell sizes dictate how many columns appear, thus creating the "mobile collapse" automatically without the need for a media query, feels like the norm. But it's hard for me to put my finger on exactly _when_ this shift occurred.

[Tomasz Jakut has written a good overview of IWD principles](https://www.smashingmagazine.com/2025/02/taking-rwd-to-the-extreme/) and why developers have incrementally adopted them over the past few years without really realizing they'd graduated from RWD to IWD. 

> I think that Jen Simmons, Heydon Pickering, and Andy Bell speak of the same thing — an approach that shifts much of the work from the web developer to the browser. Instead of telling it how to do things, we rather tell it what to do and leave it to figure out the “how” part by itself.
> 
> As Jeremy Keith notices, there has been a shift from an imperative design (telling the browser “how”) to a declarative one (telling the browser “what”). Specifically, Jeremy says that we ought to “focus on creating the right inputs rather than trying to control every possible output.”

Flexbox can lay out a few items or many items and fluidly adjust widths and spaces based on the content itself and the potential constraints which have been declared. Grid can define rows, columns, and various oddly-sized regions and then come up with the best layout for content within sizing rules. We have fluid type, container queries and units, and many other tools available now as well—even view transitions between states of content (or just straightforward pages).

I'm glad we now live in the world of IWD, even if the terminology of RWD has persisted. By combining the best of the media query world with the new fluid world, we can build ambitious and eye-catching layouts and content presentations with far less custom code than ever before.

---
date: Fri, 21 Jul 2023 12:12:56 -0700
title: Dynamic Slots
description: What if you could do even more with slots in web components?
link_url: https://www.abeautifulsite.net/posts/dynamic-slots/
---

Cory LaViska of [Shoelace](https://shoelace.style) fame writes about [an interesting data-driven technique of employing slots](https://www.abeautifulsite.net/posts/dynamic-slots/):

> Web Component authors already know how powerful slots are, but what if you could do even more with them? Here's an interesting technique to use (or abuse) slots in your custom elements. I've been calling the pattern dynamic slots.

Whether or not you end up utilizing this pattern, the fact is slots provide a truly exciting way of building layout in components which can ingest HTML content defined elsewhere. I have been championing this line of thinking lately regarding entire website layouts, and in fact this very blog uses a Declarative Shadow DOM (DSD)-based layout. Go head. Peek under the hood. `<body>` uses a shadow root. Yeah!
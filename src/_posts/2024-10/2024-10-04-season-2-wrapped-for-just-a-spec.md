---
date: Fri, 04 Oct 2024 09:39:16 -0700
title: "And That’s a Wrap! Catching Up on Just a Spec Season 2"
description: Wow did we have some juicy topics to cover on that last batch of episodes.
link_url: https://justaspec.buzzsprout.com
---

ICYMI, my good friend Ayush and myself just wrapped up the unofficial season 2 of [Just a Spec](https://justaspec.buzzsprout.com), and wow did we have some juicy topics to cover on that last batch of episodes.

Just in the past month alone:

* [Action Web Components for Full-Stack Magic](https://justaspec.buzzsprout.com/1863126/episodes/15658335-action-web-components-for-full-stack-magic)
* [Your Static Just Zapped Me](https://justaspec.buzzsprout.com/1863126/episodes/15736551-your-static-just-zapped-me) (SSG SSR CSR SPA MPA…MEAN? SSI?? PERL???)
* [Deploying Your Web App…What Could Go Wrong?](https://justaspec.buzzsprout.com/1863126/episodes/15856742-deploying-your-web-app-what-could-go-wrong)

I've been having such a blast working on this podcast, and I'm sure we'll be back for a special episode at some point before we resume in earnest next year. In the meantime, catch up on episodes you've missed, then head over to [The Spicy Web Discord](https://discord.gg/CUuYVH7Qa9) to add your thoughts, ask questions, and get to know the hosts of Just a Spec!

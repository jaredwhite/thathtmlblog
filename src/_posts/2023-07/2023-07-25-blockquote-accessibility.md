---
date: Tue, 25 Jul 2023 08:03:48 -0700
title: Blockquotes in Screen Readers
description: A demonstration of how screen readers announce various blockquote patterns.
link_url: https://adrianroselli.com/2023/07/blockquotes-in-screen-readers.html
---

Adrian Roselli is back at it again with a [detailed runthrough of how screen readers announce various arrangements of blockquote markup](https://adrianroselli.com/2023/07/blockquotes-in-screen-readers.html):

> The first four examples are lifted from WHATWG HTML’s `<blockquote>` entry. The next three are from W3C HTML’s 2019 `<blockquote>` guidance (the W3C HTML spec started redirecting to WHATWG’s version after that date).

The 7th example in the list is the one I would prefer from a coding standpoint, and it seems like that's one of the best variants for accessibility as well.

---
date: Sun, 16 Jul 2023 21:06:52 -0700
title: An Approach to Lazy Loading Custom Elements
description: Whenever a custom element appears in the DOM, load the corresponding implementation if it’s not available yet.
link_url: https://css-tricks.com/an-approach-to-lazy-loading-custom-elements/
---

Given that this blog is brand new, we have the luxury of dipping a bit into the past (like, just a few months ago!) to present you with some choice morsels.

[This article on CSS Tricks by Frederik Dohr](https://css-tricks.com/an-approach-to-lazy-loading-custom-elements/) is quite intriguing. Because of how custom elements (aka web components) work, you can actually lazy-load them with just a smidgen of fancy JavaScript:

> We’re fans of Custom Elements around here. Their design makes them particularly amenable to lazy loading, which can be a boon for performance.
> 
> Inspired by a colleague’s experiments, I recently set about writing a simple auto-loader: Whenever a custom element appears in the DOM, we wanna load the corresponding implementation if it’s not available yet. The browser then takes care of upgrading such elements from there on out.

[Shoelace recently added an autoloader](https://shoelace.style/getting-started/installation) for its component library, so maybe this technique is ready for prime time?

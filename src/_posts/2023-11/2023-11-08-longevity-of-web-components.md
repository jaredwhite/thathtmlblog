---
date: Wed, 08 Nov 2023 08:39:05 -0800
title: The Longevity of Web Components Over Frameworks
description: Why web components will outlive your JavaScript framework and are particularly well-suited to wide Markdown compatibility.
link_url: https://jakelazaroff.com/words/web-components-will-outlive-your-javascript-framework/
---

So much good stuff to unpack in Jake Lazaroff's essay [Web Components Will Outlive Your JavaScript Framework](https://jakelazaroff.com/words/web-components-will-outlive-your-javascript-framework/). Even with the opening caveat that he believes "React is a good default option", Jake starts to dive into why for his evergreen Markdown content, he decided web components were the right way to add embedded interactivity (over something like MDX or Astro components):

> Markdown has a secret weapon: you can write HTML inside of it! That means any fancy interactive diagrams I wanted to add would be just as portable as my the rest of my Markdown as long as I could express them as plain HTML tags.
> 
> Web components hit that nail square on the head. They’re a set of W3C standards for building reusable HTML elements…
> 
> Of course, I do need to keep the JS files around and link to them with a `<script>` tag. But that goes for any media: there needs to be some way to reference it from within textual content. With web components, once the script is loaded, the tag name gets registered and works anywhere on the page — even if the markup is present before the JavaScript runs.

I can identify with Jake's concern. I have gobs of Markdown content stretching back to the early days when I first began blogging with Jekyll almost a decade ago. Since then, I've switched template engines a few times, and each time any embedded components have had to be re-written (first Liquid, then ERB, then this, then that). If I want _truly future-proof Markdown content_ which is portable across all frameworks, **web components are the clear answer**.

Later on, Jake really gets to the heart of the matter:

> I’ve been building on the web for almost 20 years. That’s long enough to witness the birth, rise and fall of jQuery…
> 
> But as the ecosystem around it swirled, the web platform itself remained remarkably stable — largely because the stewards of the standards painstakingly ensured that no new change would break existing websites. The original Space Jam website from 1996 is famously still up, and renders perfectly in modern browsers. So does the first version of the website you’re reading now, made when I was a freshman in college 15 years ago…
>
> If we want that sort of longevity, we need to avoid dependencies that we don’t control and stick to standards that we know won’t break. If we want our work to be accessible in five or ten or even 20 years, we need to use the web with no layers in between. 

_No layers in between._ I love it.

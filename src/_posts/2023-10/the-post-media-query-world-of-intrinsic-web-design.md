---
date: 2023-10-19T08:50:00-07:00
link_url: https://youtu.be/-kjiyh8yfY4
title: The Post-Media-Query-World of Intrinsic Web Design
description: The best media query is the one you never need to write.
---
Just as [the best code is the code you never need to write](https://www.fullstackruby.dev/the-art-of-code/2015/01/30/the-best-code-is-the-code-nobody-writes/), the best *media query*&nbsp;is the one you never need to write.

**Intrinsic web design** is a concept which essentially represents an evolution from **responsive web design**, and what it means is that you're creating layouts and setting up responsiveness based on *layout rules* which apply based on the presentation of the content itself, rather than on total size of the browser. Imagine a grid where the number of columns is fluid, but you set up minimum and maximum column sizes so that grid cells will be positioned accordingly based on those constraints. It's a "responsive grid" yet no media queries are involved.

[Kevin Powell recently presented on his General Musings YouTube channel](https://youtu.be/-kjiyh8yfY4)&nbsp;a great explanation of what you can do with intrinsic layouts and why that's increasingly preferable to the past technique of setting up breakpoints and overriding styles in that manner. And in cases where you really do need some breakpoint-based layout logic—especially at the component level—often&nbsp;**container queries** are the way to go.

I've been full-steam-ahead on a bunch of these techniques lately, and it sure is a lot of fun!&nbsp;
---
date: Thu, 17 Aug 2023 10:25:36 -0700
title: Server-Rendering Web Components in Enhance, Lit, & WebC
description: How to spin up a simple Node server and use custom elements as templates.
link_url: https://www.spicyweb.dev/web-components-ssr-node/
---

[I mused recently](https://thathtml.blog/2023/08/node-frameworks-web-components/) about how easy (or not) it might be to spin up a simple Node server and try server-rendering some web components with the latest batch of formats like Enhance, Lit SSR, and WebC.

I ended up building a sample project with each of those three technologies, comparing the wins and the pitfalls, and providing some additional commentary on the future of HTML-first, server-first web components:

> We’ve seen various projects, most notably Lit...provide a mechanism to render web components on the server, with an optional hydration step on the client to take those server-rendered components and make them interactive after initial load. Yet, this sort of “isomorphic” architecture where client and server code is essentially identical has increasingly fallen out of step with what many developers actually want. Components which only run on the server are making a big splash across a variety of ecosystems, with even React/Next.js getting in on the action.

[Check out the full article and demos/repos on The Spicy Web](https://www.spicyweb.dev/web-components-ssr-node/) and let me know what you think!

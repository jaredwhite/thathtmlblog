---
date: Tue, 29 Aug 2023 11:08:51 -0700
title: Influential Blog Posts Throughout Web Development History
description: 25 years of professional inspiration highlighted in a single archive.
link_url: https://esif.dev/
---

Zach Leatherman of Eleventy fame recently asked folks on Mastodon to share their favorite inspirational blog posts which had made a huge impact on their careers as web developers. He then compiled many of the top hits into a permanent archive called [Educational Sensational Inspirational Foundational](https://esif.dev/).

The historical significance of some of this material cannot be overstated, so it's definitely worth your while to peruse the archive and catch up on ~25 years of web development. You might find a few familiar titles, or be introduced to entirely new (old? new again?) ideas.

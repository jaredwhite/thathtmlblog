---
date: Thu, 30 Jan 2025 13:30:02 -0800
title: Squircles on the Loose! Images Demo via the html-squircle JS Library
description: Adding a splash of pizzazz to rendering a grid of image thumbnails.
---

A few days ago [I wrote about my efforts](/2025/01/using-svg-clip-path-to-create-a-squircle/) to build a "squircle" button, where instead of using `border-radius` like a regular shlub you can make use of `clip-path` and SVG data to round the corners of an element in a more attractive manner (in technical terms, you end up with a shape called a _superellipse_).

I'm happy with how my button web component turned out, but then as I was starting to plan out a project which includes rendering a grid of image thumbnails, it got me thinking I might **squirclize** (is that a word?) the images instead of yet again reaching for _ye olde border-radius_.

Alas! The SVG path I'd used for the button was **too rounded** for the images and looked strange. After some unsuccessful attempts at tweaking the path, I once again started hunting around on the web for an alternative path.

This time, my search yielded fresh fruit! There's a 1kB (min+gzip) JavaScript library, [html-squircle](https://github.com/jdharrisnz/html-squircle), which lets you generate SVG paths based on a few parameters you supply. With this approach, you can tweak the curves in real-time until you're happy with the results!

Speaking of results, **[here's my CodePen](https://codepen.io/jaredcwhite/pen/emOXgpP)**. Doesn't that look fun? **I have a few notes** on what I did here, but in uncharacteristic fashion I'm going to put on my content marketing hat and ask you to [join the #spicy-content channel in our Discord server](https://discord.gg/CUuYVH7Qa9) where I'll divulge all the riveting details! ☺️

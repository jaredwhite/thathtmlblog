---
date: Sun, 16 Jul 2023 21:26:02 -0700
title: State of Web Components Panel (Video)
description: Zach Leatherman spoke alongside Justin Fagnani (Lit), Kristofer Joseph (Begin, Enhance.dev), and Rob Eisenberg about the current state of the Web Components ecosystem.
link_url: https://www.zachleat.com/web/state-of-web-components/
---

Zach Leatherman recently spoke alongside Justin Fagnani (Lit), Kristofer Joseph (Begin, Enhance.dev), and Rob Eisenberg about [the current state of the Web Components ecosystem](https://www.zachleat.com/web/state-of-web-components/):

> We talked a bit about the controversial is attribute, how we’re all excited about upcoming Declarative Custom Element definitions, the long term benefits of staying close to the platform, but somehow didn’t touch on adding JSX to the platform (disappointing I know).

This was a great panel discussion, I enjoyed it tremendously.

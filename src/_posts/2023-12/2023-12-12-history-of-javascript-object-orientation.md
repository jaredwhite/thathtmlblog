---
date: Tue, 12 Dec 2023 14:43:52 -0800
title: A History of JavaScript's Object Orientation
description: If you've ever felt at a loss to describe how JavaScript's "prototype-based" inheritance works and why, this is the article for you.
link_url: https://www.smashingmagazine.com/2023/12/marketing-changed-oop-javascript/
---

I [share this link](https://www.smashingmagazine.com/2023/12/marketing-changed-oop-javascript/) with a caveat: while it's an excellent look at the history of JavaScript and how it greatly differed from its namesake (the Java language) in the manner of its OOP design, it also _recommends_ current approaches to programming JavaScript today I can't say I agree with.

(Quotes like "`this` is completely avoidable in modern JavaScript" honestly make me cringe—especially when you consider the prevalence of web components which are built on top of class-based inheritance and definition patterns.)

Nevertheless, I found Juan Diego Rodríguez' article to be an enjoyable read, and it helped solidified some concepts about the prototypal origins of JavaScript's OOP I was only vaguely aware of. One of the aspects of JS I really enjoy is just how flexible its object model is and how it allows for a wide-variety of patterns, and as we can see in this article, that flexibility has been a defining feature of the language for a long time.

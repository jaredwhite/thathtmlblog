---
date: Wed, 26 Feb 2025 16:24:05 -0800
title: All the Deets on the Mighty Details Tag
description: One of the great unsung heroes of modern web UX.
link_url: https://css-tricks.com/using-styling-the-details-element/
---

The `<details>` tag is one of the great unsung heroes of modern web UX. There are so many use cases, and many longstanding patterns in GUI history can be replicated using `<details>`.

In its most basic form, you are provided with a "disclosure triangle" as it was called in Mac OS from ages ago. But you can use it for expandable sections like in a FAQ, accordions (now possible in pure HTML!), hiding & showing additional form fields based on user input, and so much more.

[Geoff Graham over at CSS Tricks](https://css-tricks.com/using-styling-the-details-element/) has written a definitive guide on using `<details>` and some of the trickier aspects of styling it and even animating the opening and closing states. I'll be sure to refer to this the next time I work on a `<details>`-based feature…which will likely be sooner rather than later!

---
date: Thu, 16 Nov 2023 14:51:27 -0800
title: The Framework Trap
description: Frameworks are eating the web.
link_url: https://www.abeautifulsite.net/posts/thoughts-on-framework-churn/
---

Frameworks are eating the web. We are awash in frontend-focused frameworks & "meta-frameworks", and migrating from one to another or carrying agnostic & repeatable patterns across frameworks is proving to be a nightmare. With every cry of "nobody ever got fired for using React"—it's drowned out by all the other nerds who'd rather use Vue or Svelte or Solid or this or that. And for plenty of understandable reasons! (_Only interop is certainly not one of them…_)

Clearly, clearly…this situation is untenable. And it's making the web demonstrably worse for both developers and users.

Cory LaViska, creator of [Shoelace](https://shoelace.style), outlines many of the very real dilemmas facing our industry in [Thoughts on Framework Churn](https://www.abeautifulsite.net/posts/thoughts-on-framework-churn/):

> We're building abstractions on top of abstractions, further locking users into those ecosystems and making churn more of a problem than ever.
>
> Sure, the box looks nice and shiny on the outside but, once you take the shrinkwrap off, there's a lot of stuff inside that won't age well. Many developers don't realize this until it's too late. Or maybe they just don't care.
>
> In this industry, you don't win by building well-architected apps. You win by shipping fast and moving on. It will be someone else's problem soon, so fuck it.

That last bit is what really grinds my gears. I suppose in a way it's job security…for the people who like to specialize in coming into legacy projects and figuring out just what the hell is going on and how to stop the madness. (I've done quite a bit of this in my career!) **But it's just a bad, bad loop.**

The answer isn't to ditch frameworks. ([Heck, I maintain one!](https://www.bridgetownrb.com)) The answer is to use tools which help fill in the gaps of DX, business logic, and fullstack programming concerns—_yet allow you to use the native platform features of the web as closely as possible_, maximizing interop and standards-based patterns as often as can be. As "vanilla" frontend specs continue to mature and apply to a wide variety of use cases and feature requirements, our frameworks shouldn't be getting in our way. They should be _introducing us_ to all the modern web has to offer. As I previously wrote about: [let me access the language](https://thathtml.blog/2023/10/let-me-access-the-language-also-container-queries/)! (whether that's HTML, CSS, or JavaScript—the real deal, not abstractions!)

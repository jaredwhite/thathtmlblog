---
date: Tue, 18 Feb 2025 19:07:32 -0800
title: Solving Unintended View Transition Wonkiness
description: A helpful resource detailing a number of examples of how things can go terribly wrong and how to fix them.
link_url: https://blog.jim-nielsen.com/2025/aspect-ratio-in-css-view-transitions/
---

**View Transitions are the bee's knees.** But you may find there can be some very bizarre visual artifacts as the "same" elements transition from one state to another. And since this technology is so new, it can be hard to find any helpful resources for troubleshooting and resolving issues with view transitions.

Which is why I'm thankful for [Jim Nielsen's research on this topic](https://blog.jim-nielsen.com/2025/aspect-ratio-in-css-view-transitions/), because in the midst of his travails, he came across _another_ article by Jake Archibald detailing a number of examples of how things can go terribly wrong and how to fix them. 🙌

Up until recently, I would have said that working on view transitions is a "nice to have" but hardly a priority for most projects since support is so new in browsers—and currently missing from Firefox entirely. But **this may be the last year when that's the case**, [as Interop 2025](https://hacks.mozilla.org/2025/02/interop-2025/) includes view transitions as one of the key focus areas. So should you start taking this seriously, and experimenting with how it can enhance your web projects? I think so.

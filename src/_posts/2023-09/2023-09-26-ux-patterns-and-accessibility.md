---
date: Tue, 26 Sep 2023 17:41:56 -0700
title: Excellent Free Resources for Accessible & Modular UX Patterns
description: From making the case for integrating a11y into your projects & teams to naming schemes for all the tokens in your design systems, these articles will really beef up your bookmarks.
link_url: https://smart-interface-design-patterns.com/articles/
---

On the side of the _Smart Interface Design Patterns_ course by Vitaly Friedman, there's [a growing collection of free articles](https://smart-interface-design-patterns.com/articles/) which I've been quite impressed by lately. A couple of my favorites:

* [How To Make A Strong Case For Accessibility](https://smart-interface-design-patterns.com/articles/accessibility-strong-case/) – if you're on the hunt for material to build a better a11y case in your project or org, this is a good one. 
* [How To Name Design Tokens in Design Systems](https://smart-interface-design-patterns.com/articles/naming-design-tokens/) – I feel like this does a fabulous job of condensing a lot of principles and patterns I've encountered over the past few years into a single summary and numerous links to read more.

Note that I haven't taken the main course myself nor read through earlier posts, but I just wanted to share these links in particular because I've really appreciated them, plus…I like the design. ☺️

---
date: Thu, 14 Sep 2023 07:49:19 -0700
title: Can we ZHUZH up the Jamstack?
description: The past and the present and where we might go from here.
link_url: https://www.youtube.com/live/BX6U8M5rjtg?feature=shared
---

That was the question on everyone's minds as we gathered under the auspices of Zach Leatherman's excellent community leadership to [discuss the present state of the Jamstack](https://www.youtube.com/live/BX6U8M5rjtg?feature=shared) and where we might go from here. Participants were:

* [Phil Hawksworth](https://www.hawksworx.com/)
* [Jared White](https://jaredwhite.com/) (who dat?)
* [Brian Rinaldi](https://remotesynthesis.com/)
* [Brittney Postma](https://brittneypostma.com/)
* and moderated by [Zach Leatherman](https://www.zachleat.com/)

I look forward to further _zhuzhing_ in the months ahead (as it seemed very much like everyone would love to keep the conversation going!), and it's refreshing to see folks with lots of history and Opinions™ in this space converse in a respectful and optimistic fashion.

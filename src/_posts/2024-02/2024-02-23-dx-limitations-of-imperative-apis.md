---
date: Fri, 23 Feb 2024 11:20:11 -0800
title: The DX Limitations of Imperative APIs
description: There's a wide spectrum of possible solutions to the challenges of utilizing DOM APIs exclusively.
link_url: "https://www.smashingmagazine.com/2024/02/vanilla-javascript-libraries-quest-stateful-dom-rendering/"
---

In [this essay at Smashing Magazine](https://www.smashingmagazine.com/2024/02/vanilla-javascript-libraries-quest-stateful-dom-rendering/), Frederik Dohr walks through some of the challenges of scaling up purely "imperative" DOM APIs inside a web component to handle complex UI state and rendering. It's a good reminder that typical developers don't blindly reach for solutions from lit-html to React simply because they seem "cool" or "trendy" but because working _exclusively_ with imperative APIs gets real old, real fast. As Frederik puts it:

> There appears to be a glaring omission in standardized browser APIs. Our preference for dependency-free vanilla JavaScript solutions is thwarted by the need to non-destructively update existing DOM structures. That’s assuming we value a declarative approach with inviolable encapsulation, otherwise known as “Modern Software Engineering: The Good Parts.”

Now I happen to think there can be a wide spectrum of approaches and third-party solutions here. Imagine vanilla JavaScript _all the way_ on one end of the spectrum, and React _all the way_ on the other end of the spectrum. There's a lot of room in the middle for light-weight tools which can address low-hanging fruit.

For example, you could define your component's base HTML inside of one or more `<template>` tags, or even template literals in JavaScript that you parse into template elements. Once all that's in place, you could make surgical DOM updates based on state updates by using [Signals](https://github.com/preactjs/signals), where a signal value change triggers an effect callback containing the corresponding DOM manipulation. This approach is actually where more and more frontend frameworks are trending: e.g., if you [peek under the hood of Vue's upcoming Vapor compiler](https://vapor-repl.netlify.app), it essentially auto-generates that style of code for you based on your use of Vue's declarative APIs.

It's possible we'll soon see primitives like signals as well as HTML templates which offer dynamically-updatable parts built right into browser APIs, and I say that with confidence because there are geniunue efforts _right now_ in the standards community to investigate both of those areas. In the meantime, we can either build libraries or reach for third-party options which offer **the most critical functionality** we need to marry state with DOM _without_ breaking our JS bundle budgets or foisting extremely-opinionated DSLs on us (like React).

The imperative DOM APIs we have today aren't "bad", they're just one level below the typical component DX we'd prefer to work with. The best abstractions will still ensure they're easy to reach for even while taking advantage of higher-level, declarative syntax.

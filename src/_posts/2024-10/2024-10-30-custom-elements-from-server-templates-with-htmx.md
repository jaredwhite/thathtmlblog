---
date: Wed, 30 Oct 2024 09:12:40 -0700
title: Minimal-JavaScript, Server-First Web Components with Declarative Shadow DOM
description: Whether you’re working with Node.js, Rails, or even C# and .NET, you’ll be able to translate these techniques to your stack.
link_url: https://tympanus.net/codrops/2024/08/20/server-first-web-components-with-dsd-htmx-and-islands/
---

My good friend [Konnor Rogers](https://www.konnorrogers.com) alerted me to this _excellent_ write-up by Rob Eisenberg all about how to [server-render web components](https://tympanus.net/codrops/2024/08/20/server-first-web-components-with-dsd-htmx-and-islands/) which use DSD (declarative shadow DOM) as well as other fun stuff in the mix like htmx and even "islands" of encapsulated JavaScript functionality.

From the tutorial:

> Over the last several years, browsers have made huge strides in bringing native components to HTML. In 2020, the first Web Component features reached parity across all major browsers, and in the years since, the list of capabilities has continued to grow. In particular, early this year, streaming Declarative Shadow DOM (DSD) finally reached universal support when Firefox shipped its implementation in February. It is this strategic addition to the HTML standard that unlocks a number of new, powerful server possibilities.
>
> In this article, we’ll look at how to leverage existing server frameworks people already use, to level up to native components, without piling on mounds of JavaScript. While I’ll be demonstrating these techniques with Node.js, Express, and Handlebars, nearly every modern web framework today supports the core concepts and extensibility mechanisms I’ll be showing. So, whether you’re working with Node.js, Rails, or even C# and .NET, you’ll be able to translate these techniques to your stack.

Yeeaaahhh! 🙌

I think it's safe to say we need approximately _one billion_ more articles like this. Just the other day, I was literally chatting with someone on Mastodon **who didn't know you could server-render shadow DOM** and appreciated the heads up on this development. Why, _why_ is this information not getting out there? I've said it before and I'll say it again: we haven't even _begun_ to see all the frameworks which will take advantage of this vital web spec. ([OK, here's one.](https://brisa.build))

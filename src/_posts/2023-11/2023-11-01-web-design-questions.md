---
date: Wed, 01 Nov 2023 14:30:39 -0700
title: Thoughts on Chris Coyier’s Answers to Questions About Web Design
description: Just for fun, here are a few of my added comments to introduce a bit of nuance…
link_url: https://chriscoyier.net/2023/10/31/answers-to-common-web-design-questions/
---

Chris Coyier, always the concise blogger, [offered a list of "no-nuance" answers](https://chriscoyier.net/2023/10/31/answers-to-common-web-design-questions/) to common web design questions. In the spirit of no-nuance, I agree with his answers!

But if I didn't and cared to quibble with a few, just for fun here are a few of my added comments:

## What font-family should I use?

> `font-family: system-ui, sans-serif;`

This is fine and dandy for demos and simple websites, and if you're starting out in the field, just do it. But for most real-world production projects, going with OS defaults is not going to help your design stand out (unless you're intentionally trying to build a "native-feeling" app), and some differences across platforms may not be visually acceptable.

## What framework should I use?

> Astro.

Probably a good default choice these days, but for static sites I also have a lot of admiration for [Eleventy](https://www.11ty.dev/) (especially with [WebC](https://www.11ty.dev/docs/languages/webc/)), and of course I'm personally partial to [Bridgetown](https://www.bridgetownrb.com) which I use for everything now (including this blog).

## What web host should I use?

> Use a host with a free plan at first, like Netlify.

I agree with the first part, but I hesitate to suggest Netlify as a go-to. I have a [variety of bones to pick with Netlify](https://www.spicyweb.dev/farewell-jamstack/)—not the least of which is their backend deployment story is far too hype-driven (serverless/edge-only) and doesn't support most fullstack frameworks. For a PaaS (Platform-as-a-Service) host that—like Netlify—is easy to use and free for static sites, as well as supports many frameworks in languages such as Rust, Ruby, Python, PHP, Elixir, JS runtimes other than serverless, etc.—or virtually anything if you want to reach for Docker—I _highly_ recommend [Render](https://render.com).

OK, that's it. Sorry for the additional nuance! 😂

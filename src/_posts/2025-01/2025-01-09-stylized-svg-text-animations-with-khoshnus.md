---
date: Thu, 09 Jan 2025 17:31:20 -0800
title: Craft Stylized Text Animations with Khoshnus and SVG
description: This JavaScript library by Amer Jabar provides a straightforward way to add Hollywood-style text animations to your website.
link_url: https://codepen.io/jaredcwhite/pen/NPKymbm
---

Have you ever wanted to see text animate onto a page with a flourish, the way you might expect when looking at titles in a well-designed video?

Well look no further, because the [Khoshnus](https://github.com/Amer-Jabar/khoshnus) JavaScript library by Amer Jabar provides a straightforward way to add those sorts of "Hollywood" text animations to your website, made possible by its use of SVG-based rendering.

The examples provided by the readme can get you started, but you'll be looking at either a React-based syntax or a straight JS function, so naturally I wanted to see if I could encapsulate Khoshnus in a web component. And the answer is…**Yes** (of course)!

[Check out my CodePen](https://codepen.io/jaredcwhite/pen/NPKymbm) to see it in action. Certain aspects of the animation have been hard-coded into the `connectedCallback` of the component, so I would want to spend more time in allowing customizations through attributes along with other clever analysis of the supplied text for positioning and delay values, but for this first pass…_it's fine_. 😅

You can pick from a variety of creative fonts Khoshnus supplies, or use your own, and there are a number of configuration options you can set to customize the appearance and animation style. I'm pretty impressed with the effects you can get with this, so I'm definitely thinking of ways I might introduce it to production projects.

(Hat tip: [CSS Weekly](https://css-weekly.com/issue-602/) for alerting me to this library!)

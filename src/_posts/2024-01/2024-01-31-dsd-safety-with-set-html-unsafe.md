---
date: Wed, 31 Jan 2024 11:15:30 -0800
title: Practice Safe DSD with setHTMLUnsafe (It’s Complicated 😂)
description: Once these APIs are widely available, you can kiss innerHTML goodbye.
link_url: https://codepen.io/jaredcwhite/pen/KKEQeBe
---

All right, so you got your web server emitting some HTML with declarative shadow DOM (DSD). Great! You fetch a fragment of this HTML, and use `innerHTML` to set an element to display this new HTML.

_Uh oh._

No worries, you think to yourself. You'll use `DOMParser.parseFromString` to construct a new fragment from the server-provided string, and then append that to your page. Surely that will work!

_Uh oh._

Yes, I'm sorry be the bearer of bad news, but the typical DOM APIs you use to turn strings of HTML into new DOM **do not parse DSD**. All those `<template shadowrootmode="…">` tags remain just that, template tags. No shadow DOM to be found anywhere.

**==Until now.==**

Say hello to `setHTMLUnsafe` (a replacement for `innerHTML`) and `parseHTMLUnsafe` (a replacement for `DOMParser.parsefromString`). These new APIs may sound scary, but that's only because they're the "unsafe" parallels to `setHTML` and `parseHTML` which are upcoming APIs providing robust sanitization features. Yes that's exactly right, HTML sanitization is coming to browsers! (But more on that at a later date.)

If you have the latest Safari Technology Preview or Firefox Beta (and possibly Chrome Canary although I haven't tried it…I know it's in the works though), [you can take a look at this functionality in this Codepen](https://codepen.io/jaredcwhite/pen/KKEQeBe).

Once these APIs are widely available, you can kiss `innerHTML` goodbye and embrace `setHTMLUnsafe` (or `setHTML`) to render strings as live HTML—shadow roots and all. And if you'd missed the news that the next production release of Firefox _will finally include DSD support_ for full progressive enhancement of web components, **rejoice my friends**. **Rejoice**.

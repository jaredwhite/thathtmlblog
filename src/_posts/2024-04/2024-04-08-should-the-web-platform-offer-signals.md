---
date: Mon, 08 Apr 2024 08:20:14 -0700
title: Should the Web Platform Offer Signals?
description: Managing reactive state updates in an application is hard, and doing it well is even harder. Can signals provide a standardized path forward?
link_url: https://github.com/proposal-signals/proposal-signals
---

That's the question being asked by a crew of researchers and spec authors led by noted web components expert Rob Eisenberg.

You know I've been talking about "signals" to all who will listen, having fallen in love with [Preact's Signals](https://github.com/preactjs/signals) implementation (and subsequently [ported it to Ruby as the Signalize gem](https://github.com/whitefusionhq/signalize)). It should be noted that the Signals library doesn't _require_ Preact—it's framework-agnostic, which makes it highly appealing for adoption by other libraries and applications.

But many people, such at the authors of a new proposal intended for submission to [TC39](https://tc39.es) (the standards body in charge of JavaScript syntax), as well as myself, believe that the web platform itself should offer something akin to signals. That is, the ability to define **observable values** (signals) which you can implicitly subscribe to in **execution contexts** (effects) which will then **re-run whenever the values change**.

From the [proposal](https://github.com/proposal-signals/proposal-signals):

> To develop a complicated user interface (UI), JavaScript application developers need to store, compute, invalidate, sync, and push state to the application's view layer in an efficient way. UIs commonly involve more than just managing simple values, but often involve rendering computed state which is dependent on a complex tree of other values or state that is also computed itself. The goal of Signals is to provide infrastructure for managing such application state so developers can focus on business logic rather than these repetitive details.
> 
> Signal-like constructs have independently been found to be useful in non-UI contexts as well, particularly in build systems to avoid unnecessary rebuilds.
> 
> Signals are used in reactive programming to remove the need to manage updating in applications.

Rob Eisenberg [published a post on Medium](https://eisenbergeffect.medium.com/a-tc39-proposal-for-signals-f0bedd37a335) to explain the proposal and how it works at a more conversational level, and the proposal also comes with a [polyfill](https://github.com/proposal-signals/proposal-signals/tree/main/packages/signal-polyfill) so you can try out the implementation today to get a feel for the shape of the API. [I put together a little CodePen of the polyfill in action here.](https://codepen.io/jaredcwhite/pen/wvZmpbP?editors=1111)

At first glance this proposal seems solid and well thought-out to me, but I don't have enough insight yet into how the solution differs internally from Preact Signals to express an informed opinion. I likely won't use the polyfill myself right away, but **I'm definitely excited** to see this proposal come to light, and I intend on tracking it _very_ closely as it starts to move through the standardization process.
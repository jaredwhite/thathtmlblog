---
date: Mon, 07 Aug 2023 08:03:19 -0700
title: The Legacy of Tailwind CSS
description: There are far better practices available to us. Writing maintainable CSS doesn't require Tailwind.
link_url: https://pdx.su/blog/2023-07-26-tailwind-and-the-death-of-craftsmanship/
---

I've been [sounding the alarm](https://www.spicyweb.dev/why-tailwind-isnt-for-me/) for [some time](https://www.spicyweb.dev/the-three-laws-of-utility-classes/) concerning the hazards of Tailwind, a project which disrespects the platform it sits on and engages in bad faith arguments with its critics. There are few tools I will outright say are conceptually incompatible with vanilla-first web development and the embrace of open web standards. Tailwind and React are both on that list.

Jeff Sandberg does [a good job in the latest entry to this corpus](https://pdx.su/blog/2023-07-26-tailwind-and-the-death-of-craftsmanship/) and why we should be concerned about what he feels is "a larger problem in development":

> There's a worrying trend in modern web development, where developers are throwing away decades of carefully wrought systems for a bit of perceived convenience. Tools such as Tailwind CSS seem to be spreading like wildfire, with very few people ever willing to acknowledge the regression they bring to our field. And I'm getting tired of it.

and a point which stood out to me in particular:

> Backend engineers have an unfortunate tendency to view frontend as "not real engineering," as something beneath them. They used to reach for Bootstrap, now they reach for Tailwind. They want to get something passable done, and go back to "real" engineering on the backend.

I've grown increasingly wary of framework proponents from Phoenix to Rails to Next.js jumping on board the Tailwind train because it's like they forgot to put on their "how to program good" hats suddenly when looking at HTML + CSS instead of Elixir or Ruby or JavaScript.

Here's the thing. **CSS is a programming language.**

And the stylesheets you write can be just as modular, as encapsulated, and as "well-named" (yes I know, hard problems of computer science and all that) as any other code you'd ever write in a good programming language.

The Rails connection is particularly vexing for me personally. Ruby is a language known for its large body of best practices and well-regarded exhortations around object-oriented software architecture and API design. Why do we _throw that all away_ just when reaching to style some HTML elements? It truly boggles the mind, and—again—shows a certain level of disrespect of the medium in which we work when building frontends on the web.

There are far better practices available to us. Writing maintainable CSS doesn't require Tailwind. Arguably it never did! And now in the year 2023, [that's more true than ever](https://justaspec.buzzsprout.com/1863126/12996379-i-just-wanna-write-css).

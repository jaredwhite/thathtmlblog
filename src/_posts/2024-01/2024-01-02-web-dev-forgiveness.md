---
date: Tue, 02 Jan 2024 14:46:48 -0800
link_url: https://blog.jim-nielsen.com/2024/fault-tolerance-html-css/
title: Better to Ask For Forgiveness Than Permission, Web Dev Style
description: HTML and CSS are designed to be fault tolerant. You can use this your advantage when prototyping.
---

Something I've always loved about the fundamentals of the web—HTML & CSS in particular—is that they're so easy to use as well as abuse. You can throw all kinds of random goofiness at browsers and they'll valiantly trudge along, doing their best to render everything despite the errors. I recognize this isn't appreciated by all, but that sort of fault-tolerance is arguably one of the reasons the web became so ubiquitous.

Jim Nielsen [frames it well](https://blog.jim-nielsen.com/2024/fault-tolerance-html-css/):

> For someone who is full of faults, like myself, I appreciate this design feature. (I mean, who doesn’t need a little extra leniency and forgiveness in their life? But that’s another blog post.)
> 
> When it comes to CSS, browsers will ignore syntax they don’t understand.
> 
> This is a feature of the language I use quite often when prototyping.

Me too!

---
date: Fri, 04 Aug 2023 07:58:39 -0700
title: Is That Lights Out for the Jamstack?
description: Thankfully there are other web hosting companies to choose from more aligned with a vision of the web I'm comfortable with.
link_url: https://www.spicyweb.dev/farewell-jamstack/
---

Earlier this week I published an [article over at The Spicy Web called My Journey Away from the JAMstack](https://www.spicyweb.dev/farewell-jamstack/) regarding my first experiences building static sites with Jekyll, why Netlify was ahead of its time when I first switched to it, and where everything went wrong from there.

> Listen, I get it. Running a successful and hopefully profitable hosting company with investors breathing down your neck is hard. I don’t begrudge them for having to pivot to enterprise cloud mumbo-jumbo in order to reel in the big bucks and justify their valuation.
> 
> But I can’t help but feel duped…like so much of the other “enshittification” we’ve been dealing with in tech over the last few years. **The cycle repeats itself**: we invest our hard-earned time and sometimes money to build on top of friendly, seemingly benign platforms—only to see those platforms wriggle out from under us and morph into something entirely different (and for our purposes, much worse).

A lively conversation then ensued on [Hacker News](https://news.ycombinator.com/item?id=36944545) with even the CEO of Netlify weighing in on the matter:

> I would actually argue that Jamstack has won to the point of basically just being "Modern Web Development" by now.
> 
> In the 7 years since I first presented the term at Smashing Conference in San Francisco, I can't think of a single new successful commercial CMS that hasn't been headless or API first. I can't think of a single new successful commerce platform that's launched in that period, that hasn't been headless or API driven.
> 
> On the contrary, big existing players have mostly changed their strategy.

I think it's quite the marketing stretch to claim that (_checks notes_) companies offering APIs for their web services is all thanks to Netlify promoting the term "Jamstack". 😄

As for "Modern Web Development" just so happening to equal whatever flavor of the month Netlify (and other "hot" names in the space such as Vercel) is talking about is sort of the whole problem with "Modern Web Development" in the first place, as I've repeatedly argued over at **The Spicy Web**.

But no matter. If Netlify's leadership is pleased with their company's performance, more power to 'em. Thankfully, as I clearly stated in my piece, there are other web hosting companies to choose from **more aligned with a vision of the web I'm comfortable with**. And while it's a huge bummer we won't have a single term like "JAMstack/Jamstack" to point to to describe a website architecture built up around "progressive generation" (aka static HTML-first, server later if ever, content editing decoupled from website deployment, etc.), perhaps that was always more of a "philosophy" of _how_ to build websites than a meaningful description of whatever results you might end up with in the final reckoning.

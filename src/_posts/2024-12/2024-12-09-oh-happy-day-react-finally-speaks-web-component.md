---
date: Mon, 09 Dec 2024 09:22:24 -0800
title: Oh Happy Day! React Finally Speaks Web Components
description: It’s possible many of the core building blocks of your UI could now be implemented as vanilla web components.
---

Hey, I'm just happy we can **finally** retire this meme _any time_ the question comes up of React supporting…y'know, the actual DOM. 😂

{: style="text-align: center"}
![It's been 84 years](/images/its-been84years-titanic.gif){: style="width: 336px"}

Look, I'm not here to give the React team kudos and attaboys. It is _ludicrous_ we had to wait as long as we did for custom elements to work correctly, and it's possible there are still some edge cases lurking (for example, custom event names with colons aren't usable via JSX props).

But with the [release of React 19](https://react.dev/blog/2024/12/05/react-19#support-for-custom-elements), our long industry-wide nightmare is over. And with these massive improvements comes the very real question:

**Not IF, but WHEN will you transition some of your React components to web components?**

Because it's now a distinct possibility that many of the core building blocks of your UI could be implemented as vanilla WCs (or even using a lightweight helper library like [Lit](https://lit.dev)), and you could just reference those directly within React and/or author higher-level wrappers around those bits. A partial migration away from React would allow you to future-proof your application for the inevitable day when you can leave React behind as chief dependency ([which should be the goal of pretty much any web application](https://infrequently.org/2024/11/if-not-react-then-what/) in _the Year of Our Lord 2025_), as well as let you utilize more of your frontend technology in a wide variety of frameworks and stacks.

I put together a couple of fun little CodePen demos showing off this new React 19 support for WCs:

* **[Merry Christmas, Mr. Bean!](https://codepen.io/jaredcwhite/pen/ogvbdqv)** — This showcases how `useState` and `useEffect` within a React component can directly modify a web component in real-time, as well as the ability to bind to custom events dispatched by the custom element.
* **[A Heartml Counter](https://codepen.io/jaredcwhite/pen/jENqXda)** — Yes, I have been chipping away on a Lit competitor of a sort, and it works with React 19 just fine! (_shh, don't tell anyone…_ 😉)

So this is pretty fantastic stuff, and I'm eager to see how open source providers of designs systems and UI libraries will take to this new world where you can just write web components and get React support "for free". I'm also curious if there's a future SSR story possible…React running on the server won't "expand" custom elements or anything like that, but that's more of a framework-level concern anyway. Surely someone, somewhere is trying to crack that nut!

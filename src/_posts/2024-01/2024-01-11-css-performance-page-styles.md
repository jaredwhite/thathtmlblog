---
date: Thu, 11 Jan 2024 14:53:05 -0800
title: Improving CSS Download Performance by Unbundling Page Styles
description: On average across many different types of pages on a website, performance is improved—particularly on slower devices/networks.
link_url: https://gds.blog.gov.uk/2023/12/15/how-we-reduced-css-size-and-improved-performance-across-gov-uk/
---

The Government Digital Service blog for the UK has [published an article about their performance gains](https://gds.blog.gov.uk/2023/12/15/how-we-reduced-css-size-and-improved-performance-across-gov-uk/) across a variety of properties, with "reductions of up to 40% in CSS size on some pages." The way they achieved these improvements is one which shouldn't come as a huge surprise but is surprisingly not on many web developers' radar (based on my own anecdotal experience discussing these sorts of issues).

The solution? Unbundling styles which are specific to certain pages and not the entire website and loading those styles on those specific pages, instead of loading all of a website's CSS via a single bundle.

From the article:

> Such improvements will mostly be imperceptible to users on high end devices or faster connections but could make a difference for users on older or low end devices (GOV.UK analytics show a sizeable number of users using Galaxy A12, A13 and earlier models) or for people on slower connections - for some pages the improvements bring load times in under 3 seconds (statistics indicate that 40% of visitors will leave a website if it takes longer than three seconds to load).

Now it should be noted that there isn't an obvious win on a few pages with extremely dense style usage—in fact there might be a _slight_ performance degradation due to the number of different stylesheets being loaded (even with HTTP/2 multiplexing). So this type of solution isn't a panacea.

Still, their findings were that on average across many different types of pages on a website, performance is improved—particularly on slower devices/networks. It's worth thinking through these points as you examine your own website's frontend architecture.

class BlogPost < Bridgetown::Component
  attr_reader :post, :post_level

  def initialize(post:, post_level: :h2)
    @post, @post_level = post, post_level
  end
end

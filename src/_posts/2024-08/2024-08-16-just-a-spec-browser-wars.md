---
date: Friday, August 16, 2024 at 8:57:57 AM PDT
title: "Browser Wars, State of JS, and the Birth of the Internet on Just a Spec"
description: In case you missed it, here’s what we’ve been talking about on the Just a Spec podcast.
link_url: https://justaspec.show
---

It's been a while since I posted here about the **Just a Spec** podcast I've been producing along with my friend Ayush Newatia, so ICYMI, we've talked about a number of notable topics recently:

* [Pendulum Swings & 2023’s State of JS](https://justaspec.buzzsprout.com/1863126/15392956-pendulum-swings-2023-s-state-of-js) – Yes, the results are in, and we talk about them…but more broadly, our feelings about the state of web frameworks in our industry and the much-ballyhooed pendulum swing back to **server-side rendering** and **HTML-first techniques** (though the rate of change is perhaps not what we might wish for).
* [The Browser Wars & an Uneasy Peace](https://justaspec.buzzsprout.com/1863126/15468655-the-browser-wars-an-uneasy-peace) – Remember Netscape Navigator? _Remember Mosaic??_ In this episode, we look back at the history of the web browser—touching on such memorable moments as the `<blink>` & `<marquee>` tags, DHTML with the launch of JavaScript, and the arrival of responsive design for mobile as well as desktop—while attempting to learn from history so that we’re not doomed to repeat it.
* [And Lo, There Was ARPANET (Baby Internet!)](https://justaspec.buzzsprout.com/1863126/15589860-and-lo-there-was-arpanet-baby-internet) – How did the Internet first begin? Why was it developed at the Advanced Research Projects Agency? Where was it initially launched at the end of the 1960s? Is it pronounced **r-OO-ter** or **r-OW-ter??** These and other hard-hitting questions are answered as we take a deep dive into the birth of humanity’s global computer network…with a dash of **90s nostalgia** thrown in for good measure.
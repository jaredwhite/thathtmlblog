---
date: Wed, 19 Jul 2023 07:59:19 -0700
title: Guest on The Vanilla JS Podcast
description: Chris Ferdinandi and I talk about Hype-Driven Development and what we can do about it
link_url: https://vanillajspodcast.com/hype-driven-development-with-jared-white/
---

I was honored to [make an appearance on The Vanilla JS Podcast](https://vanillajspodcast.com/hype-driven-development-with-jared-white/) hosted by Chris Ferdinandi:

> In today’s episode, I talk with Jared White about Hype-Driven Development, and what we can do about it.

I hope you enjoy the listen!
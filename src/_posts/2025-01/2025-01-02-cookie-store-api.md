---
date: Thu, 02 Jan 2025 16:59:50 -0800
title: A Modern Replacement for Cookie Cruft
description: Wouldn't it be nice if we had a more modern API to use for setting, reading, and deleting cookies?
link_url: https://fullystacked.net/cookiestore-api/
---

One of the oldest and cruftiest APIs in JavaScript involves cookies. In fact, can you really even say there's an "API" for cookies when it's literally just a string property on `document`? And it's very weird too, because you can keep setting the value via `document.cookie = ` and those statements add _new_ cookies rather than replace any existing ones. _wut_.

Now you could argue in this day and age we don't really need JavaScript messing around with cookies because either (a) they should be "HTTP only" meaning they're hidden from JavaScript for security reasons, or (b) we have other newer data storage options which are way better (`localStorage`, IndexedDB, etc.).

Still, there are times when we need to work with data values which are accessible on both the server and the client, and cookies are that universal mechanism. Wouldn't it be nice if we had a more modern API to use for setting, reading, and deleting cookies?

[Introducing the Cookie Store API](https://fullystacked.net/cookiestore-api/), as described by Ollie Williams on Fully Stacked:

> The Cookie Store API is a modern asynchronous alternative to `document.cookie`. The Cookie Store API has been available in Chrome and Edge since version 87, released back in 2020. The API is also available in Safari Technology Preview and Firefox Nightly. Firefox and Safari decided to implement only a subset of the API, which is what I will cover in this article.

As you might imagine, it feels intuitive to use having experienced other storage APIs, and the options you can pass to `set` offer a far more sensible approach than remembering the weird string syntax of `document.cookie`.

A polyfill is available, should you decide you want to use this syntax in production.

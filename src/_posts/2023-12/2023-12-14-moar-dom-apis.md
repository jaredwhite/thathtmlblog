---
date: Thu, 14 Dec 2023 14:53:45 -0800
title: Moar DOM APIs in 2024!
description: One of the things I love most about HTML Web Components is the focus on vanilla DOM APIs.
link_url: https://www.raymondcamden.com/2023/12/13/an-image-dialog-web-component
---

One of the things I love most about all this new talk concerning **HTML Web Components** is how much focus there is on vanilla DOM APIs.

For example, in [this demonstration by Raymond Camden](https://www.raymondcamden.com/2023/12/13/an-image-dialog-web-component), it shows how to progressively enhance a link with an image tag so that you can click on the thumbnail-sized image and see a full-size image in a dialog (using `<dialog>` of course!).

What's so cool about this trend is getting exposed to a pattern of web development revolving around inspecting, creating, and modifying real DOM nodes. No abstractions. No bespoke DSLs. No dependencies. Just utilizing what you get in a browser.

_I WANT MOAR!_ 😂 It is my most sincere wish for 2024: to increasingly leverage what's available in modern browsers and the open web platform while simultaneously minimizing my contact with tooling which hijacks my attention and pushes my skill-set in a different direction.

As Jeremy Keith keeps saying, we want to go with the grain of the web, not against it. Educators and students alike should keep these principles top of mind as the industry moves forward.

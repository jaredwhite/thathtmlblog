---
date: Thu, 16 May 2024 08:27:23 -0700
title: CSS3? Pfff. Get ready for CSS6!
description: Of course, we need to get through CSS4 and CSS5 first. Details, details!
link_url: https://github.com/CSS-Next/css-next/discussions/92
---

The arrival of **HTML5** and **CSS3** way back in the day was a tremendous milestone in the history of the web. The importance of these updated specs cannot be overstated, as they were a true "reset" in the process of how the web evolves and how standards bodies work.

But here we are in 2024, and we're still using…HTML5 and CSS3? _Hmm._ 🤔

We'll save the HTML story for another day, but regarding CSS, **clearly** the spec has evolved by leaps and bounds in recent years and there's an _incredible_ amount of power available now that wasn't available in the initial set of CSS3 features. So the **CSS Next Community Group** [has decided to do something about that](https://github.com/CSS-Next/css-next/discussions/92):

> As CSS continues to evolve with new features and specifications, the blanket term "CSS3" has become insufficient and misleading. This RFC proposes the categorization of CSS properties into distinct groups, namely CSS3, CSS4, and CSS5, to better organize and facilitate understanding of the evolving CSS landscape. This categorization aims to improve adoption and ease of teaching, while not having a direct impact towards the CSS Working Group (CSSWG) operations or the Baseline initiative.

In other words, CSS4 & CSS5 are coming! But those are simply branding labels for sets of features which have emerged since the early days of CSS3. Presumably, this can only mean one thing: **CSS6 is the future!** What might we eventually see in CSS6? Let the wild speculation begin!

But in the meantime, if you have thoughts about how CSS4 / CSS5 should be categorized, check out this RFC by the CSS Next Community Group and offer your feedback.

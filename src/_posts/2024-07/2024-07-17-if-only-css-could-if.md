---
date: Wednesday, July 17, 2024 at 11:19:28 AM PDT
title: If Only CSS Could if()
description: When this eventually lands in browsers, behold the increase of your property values!
link_url: https://css-tricks.com/if-css-gets-inline-conditionals/
---

The idea that CSS should support conditionals within style rules has been around since time immemorial.

Well, it's looking much more likely that we'll soon see the day when we can `if()` the night away (I think I'm mixing my metaphors here), now that the CSS Working Group (CSSWG) has come to a consensus around adding `if()` to an upcoming CSS specification.

[Geoff Graham over at CSS-Tricks](https://css-tricks.com/if-css-gets-inline-conditionals/) has the rundown on the various ways these conditionals could be used:

> We’re likely to see things change as the CSSWG works on the feature. But as it currently stands, the idea seems to revolve around specifying a condition, and setting one of two declared styles — one as the “default” style, and one as the “updated” style when a match occurs.

And here's a code example from Geoff of how that might look:

```css
.element {
  background-color:
    /* If the style declares the following custom property: */
    if(style(--variant: success),
      var(--color-green-50), /* Matched condition */
      var(--color-blue-50);  /* Default style */
    );
}
```

If, like me, you gaze upon this and wonder to yourself: "Self, why does this seem to do the same thing as [Conditional Style Queries](https://thathtml.blog/2024/03/superpowered-container-style-queries/)?" …well, that's because the concept is fairly similar! The difference is that a style query sits _outside_ of selectors and rules. The above would have to be written like this:

```css
.element {
  background-color: var(--color-blue-50); /* Default style */

  @container style(--variant: success) {
    background-color: var(--color-green-50); /* Updated style within query */
  }
}
```

whereas `if()` can be written directly within a property value.

I can't help but wonder though if there's more that we might get out of conditionals than this example, because I think the syntax of style queries is just fine and perhaps even better if you have a large ruleset and want to keep variations on a theme grouped together.

Still, the idea of a keyword as powerful as `if()` landing in CSS is compelling. This will be a fun feature to track as they work out all the details of the spec.




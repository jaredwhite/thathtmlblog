---
date: Mon, 01 Jul 2024 16:35:16 -0700
title: CSS Doesn't Offer Motion Blur…But It Should
description: Unless you're on a device with a superfast refresh rate, some animations can look choppy and unpleasant.
link_url: https://nerdy.dev/css-motion-blur
---

Adam Argyle [really wants motion blur in CSS](https://nerdy.dev/css-motion-blur).

Alas, it doesn't seem like much has happened on this front to spec out a solution since Adam's original proposal in 2019. And there are likely a lot of technical hurdles to overcome before this could be a thing in browser engines.

But motion blur can do so much to improve the visual appearance of animations, especially on devices which don't have a superfast refresh rate.

For example, [this CSS animation demo](https://codepen.io/hisamikurita/full/OJLrjpB) looks incredibly impressive on my iPad Pro and its 120Hz refresh rate. Most of the movements and rotations of various shapes seem smooth, with even a bit of "natural" motion blur to my eyes.

However, viewing the same demo on my Mac (still in Safari!) and my LG display, the movements are much more choppy/jittery-looking and are almost distracting because of the lack of smoothness.

I suppose one answer could just be we should all have 120Hz displays and render animations that rapidly—no need for artificial motion blur! But I don't think that's likely to be a given any time soon.

At any rate, there are no doubt many other CSS specs more pressing which need attention rather than this one enhancement…nevertheless, I sure would appreciate it if I had it!

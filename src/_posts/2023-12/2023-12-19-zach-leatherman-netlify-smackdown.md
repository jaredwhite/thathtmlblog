---
date: Tue, 19 Dec 2023 15:33:12 -0800
title: Zach Leatherman Laying the Smack Down on Netlify
description: Whatever the state of web development was in 2023, you can be sure you won't hear about it in Netlify's suspect report.
link_url: https://www.zachleat.com/web/netlify-and-nextjs/
---

I'm critical of Netlify.

Earlier this year I provided some context on The Spicy Web blog in [My Journey Away from the JAMstack](https://www.spicyweb.dev/farewell-jamstack/), and then I got a chance to speak to some of those points in the [Jamstack ZHUZH Community Round-table](https://www.youtube.com/live/BX6U8M5rjtg?feature=shared). In a nutshell, I think Netlify's marketing has become (more) intellectually dishonest in recent years. It's a shame because once I would have said Gatsby was the most intellectually dishonest framework/hosting company in terms of its marketing. Perhaps not surprising then that Gatsby was eventually absorbed into Netlify… 🫠

Thus, I appreciate Zach Leatherman of Eleventy fame [speaking up about the very suspect "State of Web Development 2023" report](https://www.zachleat.com/web/netlify-and-nextjs/) recently published by Netlify (that name alone sets my teeth on edge…and I don't mean Netlify's Edge! 😂).

Cue the references to "lies, damned lies, and statistics" here because it certainly looks like Netlify tried its darndest to come up with results which benefit its business goals (pushing Astro rather than Next.js), instead of presenting anything resembling what's been truly going on in web developer trends. I believe I share Zach's feeling that seeing Next.js take the crown in a popularity contest isn't what we'd like to see, but that's what's happening. And sure, Netlify's fudged chart throwing Eleventy under the bus certainly rubs Zach the wrong way, but it also rubs me the wrong way because I see _nothing but love_ for Eleventy in all the circles I frequent (and I've even used Eleventy a time or two now even though I myself work on the [Bridgetown framework](https://www.bridgetownrb.com)!).

Listen, neither Zach nor I have anything against Astro—far from it in fact!—but Netlify making it seem like Astro is eating the world just doesn't pass the smell test. As much as we here at **That HTML Blog** might wish otherwise, Next.js (and Vercel, let's be honest) is **crushing it** in the marketplace. Next.js may _eventually_ be dethroned by a different (non-React-based) solution, but right now it is king\*. Let's work to change that fact with sober fact-based analysis at our backs, not talking points† straight out of some corporation's biz dev unit.

\* I'm not even sure if you could argue Next.js is king in the broader landscape. All of these charts and conversations seems to conveniently miss the fact that many other fullstack frameworks in many other languages still power plenty of applications on the web. Rails, Django, Laravel, Elixir, ASP.NET—heck, even Java-based frameworks are still in vogue. And I haven't even mentioned WordPress yet!

† [Aaaand here's a reply to Zach on Mastodon from Matt Biilmann](https://mastodon.social/@biilmann/111608708779213492) proving that, once again, rot usually comes from the top. (Not the first time I've seen him [gaslight his critics](https://news.ycombinator.com/item?id=36947761).)
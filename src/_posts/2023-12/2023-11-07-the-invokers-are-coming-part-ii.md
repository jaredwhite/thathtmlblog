---
date: Thu, 07 Dec 2023 14:36:26 -0800
title: The Invokers Are Coming Part II
description: A simplified v1 is actively in the works so that the platform can implement it quickly and then move on to exciting updates.
link_url: https://github.com/openui/open-ui/pull/965
---

A few weeks ago [we reported on an initial proposal and demo](https://thathtml.blog/2023/11/invokers-are-coming/) of an exciting proposal from Open UI to provide some browser-native glue code between buttons and various actions—including ones you could write yourself.

Well implementations are quickly landing in all three browser engines, and [an initial v1 spec is taking shape](https://github.com/openui/open-ui/pull/965) with a narrower (but still very useful!) scope. Essentially it will target only popover and dialog-related actions, along with any custom actions you might want to define. Future iterations will start to add other native actions like going fullscreen or sharing the webpage or toggling another element like `<details>`.

The idea that a reasonable v1 can be spec'd, implemented, and shipped quickly and then future proposals can fuel updates to the feature-set is very exciting, and it showcases just how far we've come with regard to HTML playing a significant role in launching new web platform capabilities.

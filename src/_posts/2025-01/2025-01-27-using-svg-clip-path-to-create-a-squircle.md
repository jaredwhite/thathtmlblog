---
date: Mon, 27 Jan 2025 18:28:49 -0800
title: Using SVG Clip Path to Create a Squircle
description: A squircle offers a very gradual shift from a side to a corner, not possible with typical CSS but SVG provides a neat trick.
link_url: https://www.spicyweb.dev/i-built-a-squricle-button/
---

I wrote [a fun article for The Spicy Web](https://www.spicyweb.dev/i-built-a-squricle-button/) demonstrating a pretty neat technique for rendering "squircles" — those cooler-than-round-rect shapes we've gotten used to in Apple UIs and elsewhere, but not as common to see on the web (due to there being no immediate way to define them in CSS). Alas, `border-radius` just doesn't get us there, though perhaps someday if we get more "Houdini" features in the spec, it'll be possible.

I couldn't just stop there however, and I went on to create an `<sg-button>` web component with, well, an extra touch of panache:

> So yes indeed, we have that appealing shape without any sharpness to the curvature. But on top of that, I added an eye-catching gradient background (thanks `oklch`!) to the button—which when you hover over it, it rotates! **Whoa!** How did I do that?
> 
> That’s all thanks to the magic of `@property`, a way of defining typed CSS custom properties. The typing is crucial, because if you were to attempt to define a value like `135deg` for a new CSS custom property without using `@property`, it wouldn’t be usable for a transition. By typing it as an **angle** specifically, we gain animation functionality.

I'll admit to date I haven't spent much time investigating and experimenting with `@property`, but I certainly will now that it's available in all modern browsers and I've learned you can use it to animate gradients! (And much else besides…) 🤯 

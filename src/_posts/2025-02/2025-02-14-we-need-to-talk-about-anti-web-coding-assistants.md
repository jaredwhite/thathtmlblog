---
date: Fri, 14 Feb 2025 10:57:52 -0800
title: We Need to Talk About Anti-Web Coding Assistants
description: It's time. We can't ignore this massive threat to the continuing health of this platform we all love.
link_url: https://vale.rocks/posts/ai-is-stifling-tech-adoption
---

Look, I've managed to avoid talking about "spicy autocomplete"—aka AI coding assistants—here on this blog because I have [other](https://jaredwhite.com/tag/generativeai) [places](https://www.spicyweb.dev/surviving-the-ai-pocalypse/) in which to do so. 😅 _This is a blog about vanilla web tech, damnit!_ Let's not ruin a good time.

But I can't ignore this topic any longer on **That HTML Blog**, because…well, I'll just come out and say it. **AI is anti-web.**

There are numerous angles to take here, from the extinction-level flood of slop destroying search & social to the horrendous deleterious effects on the both the digital and natural environments—but again, this is a blog specifically about vanilla web specifications. So let's talk about that, shall we?

Or rather, I'll point you this [fabulous article by Declan Chidlow entitled "AI is Stifling Tech Adoption"](https://vale.rocks/posts/ai-is-stifling-tech-adoption). I'll let Declan outline the salient points:

> I propose that the advent and integration of AI models into the workflows of developers has stifled the adoption of new and potentially superior technologies due to training data cutoffs and system prompt influence.
> 
> I have noticed a bias towards specific technologies in multiple popular models and have noted anecdotally in conversation and online discussion people choosing technology based on how well AI tooling can assist with its usage or implementation.
> 
> While it has long been the case that developers have considered documentation and support availability when choosing software, AI’s influence dramatically amplifies this factor in decision-making, often in ways that aren’t immediately apparent and with undisclosed influence.

That's a hell of an opening. The **TL;DR** is this: _even when you are asking for code solutions based on web APIs_, you often end up getting results steering you towards React and Tailwind. 🤨 This isn't just bad on the face of it, it's doubly-bad because _even the solutions based on React and Tailwind_ are inevitably out of date.

**I was alarmed** recently when Tailwind 4 was released, which offered genuinely welcome (and let's be honest, wildly overdue) CSS-based configuration and runtime features, and the response by a number of people in the Hacker News comment thread was "we'll have to wait until the AI tools catch up to all the new syntax and methodologies".

If people are held back from adopting even newer versions of _extremely popular_ user-land web frameworks, can you imagine the issues with new vanilla web APIs which are often overlooked and ill-promoted for long stretches of time?

You really need to go read Declan's entire article because it's truly disturbing, but I'll quote one more passage:

> I’d suggest that a beginner developer, or someone creating an app exclusively via prompting, is likely to use ChatGPT due to its position in the zeitgeist and use whatever output the model first produces without much second thought – thus influencing their tech selection without their realising.

This is what most frustrates me about the idea that AI coding assistants will provide us with good solutions. The thing about programming is so often the best solutions are _the least obvious solutions_. There are very few areas in programming where the vast majority of people will all converge on similar solutions.

Whereas AI *wants* to converge on the most similar solutions. It's a hazy average of what "everyone" says or does online (according to its training data). And that's really-super-duper-bad for programming.

The best programmers come up with novel, out-of-the-box, ingenious ways of describing logic and utilizing APIs because they make the most sense for _specific_ use cases and _specific_ projects—perhaps even specific teams with specific dialects which are unique to those people in that time and place. This isn't a bug, **it's a feature of the craft of programming**.

You know how the prose writing which gets pumped out of AI is so often banal, uninspired, uninteresting, and—even when it's (accidentally) factually correct—does little to provide stick-with-you-ness the way _good_ writing does? Apply that to computer language output and _it's exactly the same problem_.

So by all means, dear developer, use AI coding assistants—and end up with **bland**, **uninspired** solutions which do a poor job of executing on the desired outcomes all while being likely based on out-of-date technologies which as Declan points out often won't even be the "native platform" way of doing things at all.

Boo, I say. **Boo.** We can, _and we must_, expect better of the tools being thrust in our faces as the future of **whatever**. 🙄

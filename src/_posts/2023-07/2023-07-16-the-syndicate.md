---
date: Sun, 16 Jul 2023 22:17:25 -0700
title: The syndicate – Adactio
description: When the current crop of services wither and die, my own website will still remain in full bloom.
link_url: https://adactio.com/journal/20323
---

It is fitting that on the launch of this new indie weblog, I quote from the man, the myth, the legend himself: [Jeremy Keith](https://adactio.com/journal/20323).

> When the current crop of services wither and die, my own website will still remain in full bloom.

I have few regrets in life, but here is one: virtually every past blog that I myself have let wither and die on the vine over the years. And it's my own damn fault.

_No longer._

Here's to letting our websites remain in full bloom. 💐

**That HTML Blog** will stay on the internet for as long as I stay on it. Promise. [Let's do this.](/about)

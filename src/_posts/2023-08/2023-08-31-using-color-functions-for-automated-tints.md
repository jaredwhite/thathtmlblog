---
date: Thu, 31 Aug 2023 14:43:37 -0700
title: Using CSS Color Functions for Automated Tints
description: We can derive new colors from base colors now in vanilla CSS. How thrilling!
link_url: https://www.abeautifulsite.net/posts/better-buttons-with-color-mix-and-custom-properties/
---

It really feels like for the longest time, you couldn't do anything to derive new colors from a base color using vanilla CSS. Sure, you could use a preprocessor. But, gosh, we want our code to run in a browser! And work throughout a design system with user-customizable variable tokens!

Thankfully, the future has finally arrived this year in the form of `color-mix()`, and to help us get a sense of what's possible, [Cory LaViska has the lowdown](https://www.abeautifulsite.net/posts/better-buttons-with-color-mix-and-custom-properties/):

> Let's build a button that accepts one color and calculates its hover and focus states automatically. For this experiment, we'll use CSS Custom Properties, color-mix(), and OKLCH to ensure that tints and shades are perceptually uniform.

It's BUTTON TIME! 🔤

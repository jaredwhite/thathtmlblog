---
date: Fri, 10 Nov 2023 08:16:01 -0800
title: The Invokers Are Coming
description: Yet another game changer that will make building interactive UI on the web oh so much easier.
link_url: https://open-ui.org/components/invokers.explainer/
---

The fine folks at Open UI have been working hard on a lot of exciting new proposals to make building web interfaces easier, and [my absolute favorite so far has been Invokers](https://open-ui.org/components/invokers.explainer/).

You know how in every interactive exchange on the frontend, you have to "wire up" something (like a button) to make something else happen? Whether imperatively or declaratively, that typically means setting up a click handler to call a method on a component or some other function. But…_what if I told you_ the web may soon give you a **native feature** to handle this "wiring up"? And at least some of the time, you'd never need to write a lick of JavaScript to use it?

That's what **invokers** are for. And [here's a CodePen showing this off](https://codepen.io/keithamus/pen/abXbzqv) (you'll need something like [Firefox Nightly](https://www.mozilla.org/en-US/firefox/channel/desktop/#nightly) with a flag switched on—go to `about:config` for that and search for "invokers"). In this example, an `<h1>` tag of all things is listening for the `invoke` event, and when you click the various buttons which point to that heading using `invoketarget` and `invokeaction`, _stuff happens_. It feels positively magical!

Many native HTML elements will know how to handle invoke events automatically. For example, you could point to a dialog box as the invoke target, and by using `invokeaction="close"`, _your button will close the dialog_. Like, it will Just Work™. No JavaScript required, not even an `onclick=`. Heck, you could have another button which opens the dialog in the first place, also with zero JS.

Between the zero JS built-in use-cases and the ones you can write yourself for your custom components, I think it's obvious that the invokers mechanism—should it eventually roll out to all browsers mostly along these lines—is an **absolute game changer** for how interactive UI on the web gets built. And yet another nail in the coffin of needing bespoke frameworks & DSLs to accomplish common tasks. _I am incredibly, incredibly excited about this!_

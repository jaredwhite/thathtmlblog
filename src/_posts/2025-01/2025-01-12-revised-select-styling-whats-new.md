---
date: Sun, 12 Jan 2025 16:00:59 -0800
title: "Select Styling Revisions: What’s New"
description: We’re getting closer to a shipping version of the spec, and that’s very exciting.
link_url: https://una.im/select-updates/
---

A few months back, [I'd reported on `<selectmenu>` turning back into a regular `<select>` tag](https://thathtml.blog/2024/09/new-select-styling-proposal/)—but with awesome new CSS-based smarts for completely customized styling.

Since then, there have been some revisions to the proposed spec, and helpfully we have a [thorough rundown of what's changed courtesy of Una Kravets](https://una.im/select-updates/):

> A lot has changed! And for the better. There were a few things re-named, and significant changes to the user agent stylesheet. Now that this feature has evolved and is hopefully close to shipping, here is a list of updates since the RFC.

There's definitely plenty to like here, though I must admit it's a little confusing to have a `<button>` element inside of `<select>` yet you'd still be styling `<select>` and not `<button>` to customize the appearance of the control. I "get it", but I wonder if folks coming to this in the future will try applying styles directly to `<button>` and…it doesn't work?

Regardless, I'm at the point already where I just want this to get shipped and be available everywhere because **it's totally rad**. 😎

---
date: Wed, 20 Sep 2023 15:09:13 -0700
title: How to Bring Reactivity to Your JavaScript Code
description: Patterns and native web API demonstrations to help your vanilla JavaScript "react" to state changes.
link_url: https://frontendmasters.com/blog/vanilla-javascript-reactivity/
---

I meant to post a link to this article the first moment I read it, because _it's just so good._ ⭐️

Marc Grabanski of Frontend Masters writes in [Patterns for Reactivity with Modern Vanilla JavaScript](https://frontendmasters.com/blog/vanilla-javascript-reactivity/):

> As an industry, we associate reactivity with frameworks, but you can learn a lot by implementing reactivity in pure JavaScript. We can mix and match these patterns to wire behavior to data changes.
> 
> Learning core patterns with pure JavaScript will lead to less code and better performance in your web apps, no matter what tool or framework you use.

Amen to that!

There are so many great examples here, from how to work with custom events in and even outside of the DOM, how to use observables and proxies, a basic implementation of signals and effects (they're so rad), MutationObserver, and a whole lot more. I recommend grabbing your favorite beverage and settling down to read all the way through!

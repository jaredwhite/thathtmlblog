---
date: Thu, 20 Jun 2024 15:13:32 -0700
title: htmx 2.0 is Here
description: Only a few marquee features of note, but a plethora of improvements of its internals.
link_url: https://htmx.org/posts/2024-06-17-htmx-2-0-0-is-released/
---

It seems weird to start a headline lowercased, but that's how htmx likes to roll!

So [version 2 is here](https://htmx.org/posts/2024-06-17-htmx-2-0-0-is-released/), and by and large it seems largely focused around modernizing internals rather than offering specific new features, but nevertheless there are a few improvements. The two which stand out to me are better support for various bundling strategies if you use the NPM package (ESM is the most attractive of course), and a way to opt-into htmx's functionality with shadow DOM in web components. It's gratifying to see libraries like this take shadow DOM seriously.

As an aside, the State of JavaScript 2023 survey results just came out (I'm sure I'll be sharing my general perspective on that in an upcoming episode of the [Just a Spec Podcast](https://justaspec.show)), and I was [intrigued to see htmx show up](https://2023.stateofjs.com/en-US/libraries/front-end-frameworks/) in the front-end frameworks section. Usage numbers aren't high, but both interest and retention are. Clearly htmx is growing in popularity, as it has received a fair amount of buzz as of late.

Hopefully the philosophy and approach of htmx will foster increased attention on HTML-first development techniques, regardless of whether projects end up using htmx specifically.

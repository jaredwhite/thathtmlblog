---
date: Mon, 28 Aug 2023 18:47:14 -0700
title: Open Props Wins Award from State of CSS 2023
description: According to the survey, it's the technology with the highest percentage of returning users.
link_url: https://2023.stateofcss.com/en-US/
---

The [results are in for the 2023 State of CSS survey](https://2023.stateofcss.com/en-US/), and among many surprising (and some not so surprising) takeaways is that the honor of winning the "Highest Retention" award went to…[Open Props](https://open-props.style)!

According to State of CSS, this award goes to "the technology with the highest percentage of returning users", with runners-up being [CSS Modules](https://github.com/css-modules/css-modules) and [UnoCSS](https://unocss.dev). Congratulations to Adam Argyle and crew! 🎉 (If you haven't yet tried out Open Props, I can unequivocally recommend it.)

I have a **slew** of thoughts about the survey results and will be talking a lot about them here, over at [The Spicy Web](https://www.spicyweb.dev), and on the [Just a Spec Podcast](https://justaspec.show). I see lots of encouraging signs here on numerous fronts, and to offer a quick summary before the deep dives to come:

**Vanilla CSS is winning.** 🚀

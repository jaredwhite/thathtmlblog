---
date: Thu, 27 Jul 2023 13:48:13 -0700
title: Building a Greener Web (Video)
description: This an important topic we should all be thinking about.
link_url: https://css-irl.info/video-building-a-greener-web/
---

Michelle Barker over at CSS in Real Life [shares a presentation she gave on environmental sustainability](https://css-irl.info/video-building-a-greener-web/) in web development & production:

> Although it may seem like a big, scary topic, there’s a lot we can do in terms of mitigating environmental harm in our own work, sharing knowledge and influencing the industry as a whole. I hope you’ll find some useful takeaways here, and be encouraged to join the fight against climate change.

How "green" is my website is honestly not something I've given much thought to, but I definitely should. (Thankfully this blog seems to rank pretty well [according to Website Carbon Calculator](https://www.websitecarbon.com/website/thathtml-blog/), and generally any static site will fare far better compared to one which requires a dynamic application layer and database/API access.)

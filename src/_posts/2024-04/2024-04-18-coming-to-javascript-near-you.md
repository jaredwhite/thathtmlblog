---
date: Thu, 18 Apr 2024 12:33:36 -0700
title: Coming Soon to JavaScript Near You? (TC39 Stage 2 Proposals)
description: It'll definitely be a while before we see some of these getting prototyped in browsers, but it's always fun to take a sneak peak at the future of this vital web language.
---

With the [recent talk about Signals](https://thathtml.blog/2024/04/should-the-web-platform-offer-signals/) entering TC39 Stage 1, I thought it'd be interesting to look through other TC39 proposals which are a little farther ahead in [Stage 2](https://github.com/tc39/proposals?tab=readme-ov-file#stage-2) and see what might be coming down the pike.  

This isn't an exhaustive list, just the ones which caught my eye:

* [Collection Normalization (Key/Value Coercion)](https://github.com/tc39/proposal-collection-normalization) adds a coerceKey and coerceValue parameter to Map to affect how new items get added.
* [Records & Tuples](https://github.com/tc39/proposal-record-tuple) provide Object & Array-like immutable data structures which can only contain primitives.
* [Module Declarations](https://github.com/tc39/proposal-module-declarations) essentially makes it easier for bundlers (or hand-written code for that matter) to place multiple ES modules within a single file. Code farther down in the file could import code from a module higher in the file, and other files could import from one or more of those modules.
* [Pipeline Operator](https://github.com/tc39/proposal-pipeline-operator)…as someone who's a _massive_ fan of pipelines (and who hacked together [solutions like this for Ruby](https://www.serbea.dev)…[twice!](https://www.serbea.dev/#add-pipelines-to-any-ruby-templates)), I think this is an **fabulous idea**.

I almost added an entry for [Range](https://github.com/tc39/proposal-iterator.range), but honestly I don't care for this proposal. As a language feature, it feels poorly executed compared to the glory that is ranges in Ruby, Swift, and some other languages. Either do it right, or don't do it (IMHO).

But at any rate, there are a number of [really interesting proposals](https://github.com/tc39/proposals?tab=readme-ov-file#stage-2) in the works for the future of JavaScript, and we'll take a look soon at Stage 3 proposals which are even farther along. My favorite of that bunch? [Import Attributes](https://github.com/tc39/proposal-import-attributes?tab=readme-ov-file). 😍

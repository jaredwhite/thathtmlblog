---
date: Tue, 18 Jul 2023 08:19:02 -0700
title: Chromium browsers shipping CSS Subgrid in September
description: The latest word from Bramus
link_url: https://front-end.social/@bramus/110732210507902533
---

Bramus at Chrome Developer Relations [says CSS Subgrid will be shipping in Chrome 117](https://front-end.social/@bramus/110732210507902533):

> While I was out on vacation, CSS Subgrid got enabled by default in Chrome Canary 117 🥳
>
> Chrome 117 will be released as stable mid-September.

[Here's a codepen](https://codepen.io/bramus/full/OJaZVye) to check if your browser supports CSS Subgrid.

(Fun fact: this feature originally shipped in Firefox in 2019! It only came to WebKit browsers last year, and now finally to Chromium browsers later this year.)
---
date: 2023-10-05T10:28:00-07:00
link_url: https://www.flexer.dev
title: Weird Flexbox, But OK
description: >-
  If you get confused by flexbox and need a bit of visual assistance, this is
  the tool for you.
---
Flexbox is powerful and even a little fun, but it can also be quite confusing if you haven't worked with it a whole bunch and mentally made the connections between the different flexbox rules and what happens visually.

[Flexer](https://www.flexer.dev) is a tool to help you get over that hurdle by providing simple controls around justification, alignment, gap, and so forth to help you get a block of flexbox CSS which looks the way you want it. You can experiment with multiple items of different sizes to make sure everything is working correctly, and see tooltip explanations of what different flexbox rules can do. I certainly learned a thing or two checking this out!
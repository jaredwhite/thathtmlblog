---
date: Thu, 14 Mar 2024 11:10:56 -0700
title: More Fun with Web Components and MutationObserver
description: An API which is so powerful it can beat at the heart of entire JS frameworks.
link_url: https://www.raymondcamden.com/2024/03/13/responding-to-html-changes-in-a-web-component
---

Sometimes components need to know what's happening inside of them. Whether you're working with the concept of "slots" or just plain ol' HTML, you may need to react to changes imposed from without. Custom elements can of course monitor attribute changes via the `attributeChangedCallback`, but what about changes to children in the DOM? That's where things start to get murky.

Thankfully, the native platform provides a solution in the form of `MutationObserver`, an API which is so powerful it can beat at the heart of entire JS frameworks (Stimulus being but one example).

[Raymond Camden provides an example of an image counter component](https://www.raymondcamden.com/2024/03/13/responding-to-html-changes-in-a-web-component) which reacts to the number of `<img />` tags inside of the component. Add or remove a tag through any means (JavaScript manipulation, monkeying around in dev tools, swapping out an interior HTML fragment…anything!) and the component's counter will update near-instantaneously.

Some prior art from yours truly—[in a video over at The Spicy Web](https://www.spicyweb.dev/videos/2023/slotted-mutations-in-web-components/) I demonstrated how to pull a light DOM `<table>` tag out of a shadow DOM slot and then react to the number of rows in the table using `MutationObserver`. We (Ayush and I) also talked about this awesome API along with some other favs [in an episode of Just a Spec](https://justaspec.buzzsprout.com/1863126/13680822-fast-frontend-testing-storage-observers-fetch-more).

Needless to say, this API rocks. If you're one to build components in a mostly "vanilla" manner, you'll likely be reaching for `MutationObserver` on more than one occasion.

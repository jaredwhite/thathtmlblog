---
date: 2023-10-17T13:15:00-07:00
link_url: https://youtu.be/eTTdl5CZDlo?feature=shared&t=2476
title: Let Me Access the Language! (also, Container Queries!)
description: >-
  Whoops! Your abstraction layer is getting in the way of my access to the web
  platform.
---
On a [recent episode of Learn with Jason, Miriam Suzanne got to demonstrate all the cool stuff](https://youtu.be/eTTdl5CZDlo?feature=shared&amp;t=2476) you can do with container queries (and CSS grid!) and how browser specs finally got us to this point. But…that's not what this post is about. 😉

41 minutes into the video, Jason goes on a lovely rant about the dangers of abstraction layers when they&nbsp;*remove*&nbsp;you from gaining clear access to the languages of the web. *Especially* when it comes to CSS, as we're now beginning to see an inversion (as Miriam explains) where the pace of the native platform is actually *eclipsing* the pace of many of the popular tools attempting to wrap CSS. (And I would argue, widespread education in this space is a&nbsp;**huge**&nbsp;problem—people are arguing over what CSS can or can't do using years-out-of-date concrete examples!)

**The bottom line: let me access the language!** Don't become so enamored of your abstractions and so beholden to your tooling's technical debt that you fail to provide clear access to the native web platform. Whether we're talking about HTML, CSS, or JavaScript—if I can't use a new feature in any of those technologies because your abstraction has gotten in my way…well, we have an issue.

Things weren't always like this! There was a time when the capabilities of the web evolved *very slowly*. We&nbsp;*had*&nbsp;to use user-land abstractions to paper over all the weaknesses in the underlying platform. **The good news is**, nowadays the&nbsp;*reverse*&nbsp;is often true! The web is evolving rapidly, and the best libraries & frameworks are the ones which can&nbsp;**continuously align with current & future web standards**.
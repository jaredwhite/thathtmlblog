---
date: Mon, 25 Sep 2023 10:26:16 -0700
title: "First Ever State of HTML Survey is Now Live"
description: We've had State of X surveys for all sorts of web technologies. Isn't it strange we've never had one for HTML?
link_url: https://lea.verou.me/blog/2023/state-of-html-2023/
---

Isn't it strange we've never had a <q>State of HTML</q> survey before? [CSS gets one.](/2023/08/open-props-wins-2023-state-of-css-survey/) Lots of other web technologies get one. But not poor ol' HTML. [Until now!](https://lea.verou.me/blog/2023/state-of-html-2023/)

Lea Verou headed up the project to design and launch a new survey for HTML, and says it was "likely the most ambitious Devographics survey to date". The benefits of taking the survey include:

> Survey results are used by browsers to prioritize roadmaps…  
> Learn about new and upcoming features you may have missed  
> Get a personalized score and see how you compare to other respondents  
> Learn about the latest trends in the ecosystem and what other developers are focusing on

I took the survey and was surprised (pleasantly so I might add) that I learned about a ton of stuff I wasn't aware of! I suspect that will be the case for many. Hopefully if a lot of people [take the survey](https://survey.devographics.com/en-US/survey/state-of-html/2023/?source=leaverou), we'll gain greater insight into what folks don't already know about HTML and what they're hoping for in the future of the spec.

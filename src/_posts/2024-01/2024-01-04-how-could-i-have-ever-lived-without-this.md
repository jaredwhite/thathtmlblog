---
date: Thu, 04 Jan 2024 12:24:15 -0800
title: How Could I Have Ever Lived Without This…
description: It's worthwhile to take stock of all that we have today on the web platform.
link_url: https://matthiasott.com/notes/whats-too-good-to-be-true
---

Matthias Ott asks the very good question: [What’s Too Good to Be True?](https://matthiasott.com/notes/whats-too-good-to-be-true)

> Which of the [web platform] features that got shipped in the last few years have actually proved to be the most valuable? Which features do you use all the time? An interesting question to think about for a little while…many of the features we now take for granted actually almost sound “too good to be true”.

As grumpy developer nerds, we can spend lots of time complaining about and arguing about the features we _don't_ have on the platform yet (or haven't yet rolled out in all browsers), but it's helpful to take stock of **what we do have today** and appreciate all the progress made thus far.

Matthias mentions such modern stalwarts as `min()`, `max()`, and `clamp()`, the `lazy` attribute, and `:where()` and `:is()`. All good stuff!

I'll throw a couple out which have just landed or will _very soon_ in Firefox thus completing the rollout: `:has()` and declarative shadow DOM. I'm also a huge fan of `color-mix()` which landed everywhere last year—a great way to produce new colors based on variables & values without any reliance on preprocessors such as Sass.

What are _your_ modern favorites?

---
layout: page
title: Contact the Blog
---

Hey there! If you haven't already, I suggest you read the [About page](/about) to get a feel for what this site is all about.

If you'd like submit a link for consideration on the blog, awesome sauce! Simply email <a href="mailto:<%= site.metadata.email %>"><%= site.metadata.email %></a> with the URL, title, and a one paragraph summary of the link. (I reserve the right to edit as needed.) Topics may include:

* **HTML**, along with any templating languages which are _very lightly_ sitting atop the markup standard.
* **CSS**, along with any tools which are _very lightly_ sitting atop vanilla CSS (such as PostCSS).
* **JavaScript**…that is, standard "ECMAScript". TypeScript will only be featured if it's in JSDoc form (aka, real JavaScript + type hints in comments which then are compatible with the TypeScript ecosystem).
* Discussion of **Web Components** is very much welcome as a standards-based component format for the web.
* Open protocols & formats such as **HTTP(S)**, **XML**, **RSS/Atom**, **ActivityPub**, etc.
* General techniques which are related to the practice of vanilla & HTML-first web development.

Topics may not include:

* React (the most standards-hostile framework in widespread use today).
* Tailwind (also the most standards-hostile framework in widespread use today).
* Proprietary cloud platforms with no hope for escape (such as AWS).
* Extremely complex build toolchains which require highly-specialized programming knowledge.
* Any other tools which steer people away from standards-aligned web technology and towards vendor-specific DSLs and service lock-in.

I very much look forward to your submissions! ✌️

----

If you have any other questions, **please do get in touch with me!** You can find me in the fediverse [@jaredwhite@indieweb.social](https://indieweb.social/@jaredwhite), or you can join [The Spicy Web Discord](https://discord.gg/CUuYVH7Qa9) where I—along with a friendly bunch of vanilla-first web developers—have gathered to share ideas, tips, and bugbears. I hope you decide to join us!

---
date: Fri, 19 Jan 2024 11:03:07 -0800
title: XSLTs Are Back, Baby!
description: Styling XML news feeds like it's 2004.
link_url: https://rknight.me/blog/styling-rss-and-atom-feeds/
---

A statement you no doubt are shocked to hear uttered in the year 2024. 😂

So why are we styling XML news feeds like it's 2004? It's because some bloggers are sick of hiding their RSS/Atom feeds away where no one can find them…and then when they do, browser treat them as ugly text files—or worse, don't let you do anything useful with them at all. It's more confusing than helpful, and in an age where we desperately need to claw back control of publishing on the open web, making news feeds feel friendly and inviting is actually pretty smart.

[Robb Knight explains how he added styling to his feeds](https://rknight.me/blog/styling-rss-and-atom-feeds/) via an XSLT stylesheet (That's eXtensible Stylesheet Language Transformation for you youngsters out there), which is just a fancy way of turning XML data into HTML which you can style with regular ol' CSS.

> This is much nicer than showing a generic XML file or worse like on iOS where it tries to open the file in whatever application has decided it handles feeds.

You can try these out on [Robb's Subscribe page](https://rknight.me/subscribe/). Guess it's time for me to work on styling our Atom feed here on **That HTML Blog**…

**Fun Fact:** One of my first big tech industry jobs in the early 2000s was working with XML and XSLT files, and I'm not ashamed to admit I still love these formats. Cool to see a bit of a resurgence with this sort of use case.

---
date: Thu, 03 Oct 2024 14:33:02 -0700
title: Crafting Visual Art with CSS Custom Properties
description: Celebrating a medium that allows for rapid experimentation and results which can easily be refactored or redesigned.
link_url: https://css-irl.info/limitation-breeds-creativity/
---

I've always loved writing CSS and making attractive layouts on the web, but I never had much of an eye for patterns and shapes and other forms of visual art. So I'm always delighted when I come across creative uses of CSS in these sorts of projects.

Michelle Barker demonstrates how she came up with a sort of paper collage vibe encompassing different lines, shapes, and colors and then—[behold the magic of custom properties](https://css-irl.info/limitation-breeds-creativity/)!

> To prevent myself getting carried away and spending hours tweaking things, I decided to limit myself in a similar way to having just a few scraps of paper to play with. Each composition has the same base colour background, and same three background gradients that can be adjusted in a limited way with custom properties. The colours can’t be varied, only a few details like the size and position of the circle (made with a radial gradient), or the angle and width of the linear gradient. No additional elements or pseudo-elements are allowed. Here’s the result!

I love how CSS custom properties / variables so easily let you design your own bespoke API for any sort of creative work you need repeat more than once. And now that the Properties & Values spec (aka `@property`) just entered **Baseline 2024**, there's even more wizardry possible. We'll be sure to cover some of that here soon.

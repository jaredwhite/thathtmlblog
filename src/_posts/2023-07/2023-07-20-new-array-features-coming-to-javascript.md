---
date: Thu, 20 Jul 2023 09:26:38 -0700
title: Array to Object/Map Grouping is Coming to JavaScript
description: I'm excited to see this land in browsers soon.
link_url: https://fullystacked.net/posts/grouping-array-items-with-groupby/
---

Ollie Williams [writes over at Fully Stacked](https://fullystacked.net/posts/grouping-array-items-with-groupby/):

> The first argument to `Object.groupBy` is an array or any other iterable. The second is a callback function. This example separates numbers into two groups depending on whether they are odd or even.

The example being:

```js
const randomNumbers = [24, 32, 49, 10, 9, 7, 1082, 88, 101]

const groupedNumbers = Object.groupBy(randomNumbers, num => {
  return num % 2 === 0 ? "even" : "odd"
})
```

I'm excited to see this land in browsers soon. I use Ruby's similar `group_by` array method a lot, and it's really helpful when working with a variety of data structures and patterns.

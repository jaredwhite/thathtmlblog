---
date: Thu, 03 Aug 2023 09:47:49 -0700
title: This Podcast is Lit AF! 🔥
description: Justin Fagnani joins the JS Party podcast to talk about Lit, a library that helps you build web components.
link_url: https://changelog.com/jsparty/284
---

Justin Fagnani [made a memorable appearance on the JS Party podcast](https://changelog.com/jsparty/284) to talk all about the world's favorite (I would venture to say) web component base class: Lit.

> Justin Fagnani joins us this week to talk about Lit, a library that helps you build web components. With 17% of pageviews in Chrome registering use of web components, Lit has gained widespread adoption across a variety of companies looking to create reusable components which leverage the power and interoperability of the web platform. Tune in to learn about what makes this tiny library so incredibly lit!

It's a great episode to listen to regardless of if you're new to Lit or you've already been using it for a while. It's also an impressive achievement that, in terms of NPM downloads, the library is [now nipping at the heels](https://npmtrends.com/@angular/core-vs-lit-vs-preact-vs-svelte-vs-vue) of some of the biggest frontend packages in web development such as Preact and Angular and is ahead of Svelte.

---
date: Tue, 04 Feb 2025 20:06:58 -0800
title: "Live CSS Colors: What You Can Safely Use"
description: We're not quite there yet, but the day is coming when all you'll need for defining a fantastic color palette is a handful of base colors.
---

One of the most exciting developments in Modern™ CSS is the ability to generate new colors in real-time programmatically based on variables or the `currentColor` keyword.

However, there are two separate ways to modify or derive colors, each with its own set of goals and syntax, and the confusing thing about them is they have rolled out at different paces.

The first method you should know about is `color-mix`. It's one I've been using for some time now, and it was the first one which became widely available in 2023. [Can I Use reports 91.93% support](https://caniuse.com/mdn-css_types_color_color-mix) which is quite impressive.

The second method is newer, called Relative Color syntax. This syntax has rolled out in a more incremental fashion, and if you're looking for `currentColor` support in particular (more on that in a moment), [Can I Use reports only 77.53% support](https://caniuse.com/css-relative-colors) which in my opinion means it's not quite ready for widespread production use.

The reason using `currentColor` with color functions is so exciting is you could define a base "text" color for a component—say a badge—and then derive a lighter/more transparent background or a drop shadow based on that. [Ollie Williams shows off a demo here](https://fullystacked.net/currentcolor-with-relative-color-syntax/) using Relative Color syntax which is pretty awesome.

[Here's a demo on the 10 Pound Gorilla blog](https://10poundgorilla.com/Learning-Lab/Article/how-the-color-mix-css-feature-can-help-simplify-your-sites-colors) using the `color-mix` function for on-the-spot dynamic theming and a combination of CSS variables for the container & text along with a variable/`currentColor` mix for link colors.

**Bottom line:** as you wade into defining live colors using variables and/or `currentColor`, I would recommend seeing if you can go with `color-mix` as a starting point. If that doesn't work, you can try Relative Color but bear in mind that if you need more than just CSS variable support, aka `currentColor`, browser support isn't quite there yet.

But the exciting news is we're pretty close to ubiquity on this! The day will soon be here when all you'll need for defining a fantastic color palette along with a variety of shades and contrasts is a handful of base colors—even ones specified in real-time. Hardcoding dozens or even hundreds of colors as CSS variables—while impressive in its own right—will largely be a thing of the past.

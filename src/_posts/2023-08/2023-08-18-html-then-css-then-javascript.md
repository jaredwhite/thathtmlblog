---
date: Fri, 18 Aug 2023 11:30:49 -0700
title: HTML, Then CSS, Then JavaScript
description: When you run into problems, it's because you tried to reorder these things or combine them in weird ways.
link_url: https://blog.jim-nielsen.com/2023/meaning-in-web-tech-stack-ordering/
---

Something I talk about _all the time_ and y'all will get sick of me talking about it, is this order of development operations:

1. First HTML.
2. _Then_ CSS.
3. _Then_ JavaScript.

Jim Nielsen writes about this very thing in [There’s Meaning in the Ordering of the Web’s Tech Stack](https://blog.jim-nielsen.com/2023/meaning-in-web-tech-stack-ordering/)—which just so happens to itself link to [a wonderful presentation by Zach Leatherman all about web components](https://www.youtube.com/watch?v=R4Ri4ft7bXY) at JSHeroes 2023, so consider this post a two-for-one deal! 😄

Jim:

> There’s value, performance, and accessibility to be gained from following the subtle meaning conveyed in the ordered list of the web’s technologies.

Couldn't have said it better.

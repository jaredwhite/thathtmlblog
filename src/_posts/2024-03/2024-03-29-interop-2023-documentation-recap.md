---
title: MDN’s Recap of Interop 2023 from the Documentation Angle
description: Enabling developers to confidently build for the web using stable cross-browser functionality.
date: Fri, 29 Mar 2024 08:54:28 -0700
link_url: https://developer.mozilla.org/en-US/blog/interop2023-mdn-doc-updates/
---

It goes without saying that **Interop** is one of the best things to have happened to the implementation process of new web standards. Browser vendors and spec authors are working together more closely than ever—not simply to check a box eventually, but to ensure that the implementations are trustworthy, perform as expected across platforms—and perhaps most importantly, _documented correctly_.

**Brian Smith**, Staff Technical Writer on the MDN Web Docs team, [has provided a recap](https://developer.mozilla.org/en-US/blog/interop2023-mdn-doc-updates/) of the technical documentation process for **Interop 2023** and what we can look forward to in 2024:

> The role of the MDN team in Interop involves ensuring that there is reliable and up-to-date documentation for the features included in Interop, especially for those that have reached cross-browser support. This enables developers to confidently build for the web using functionalities that have recently become stable across major browser releases.
>
> We're excited to see what's landing on the web platform in 2024, and we're going to be keenly watching Interop to see what the next movers and shakers are. We see Interop as one of the most important and exciting initiatives that motivates browser vendors to build a compliant and open web.

**Absolutely!**

---
date: Thu, 15 Feb 2024 06:52:37 -0800
title: Variable Font Face Made Easy
description: If you want to start playing with variable fonts in your designs, this article will point you in the right direction.
link_url: https://css-irl.info/how-i-solved-my-font-rendering-problem/
---

I've set up `@font-face` rules and styling for variable fonts in the past, but I recall dealing with some fiddly bits that sort of turned me off from reaching for variable fonts as a default approach.

Well, [in this example](https://css-irl.info/how-i-solved-my-font-rendering-problem/) of how Michelle Barker got things set up for her site in a recent redesign, it seems very straightforward and makes a lot of sense. For example:

> It’s recommended that we set 'woff2-variations' as the font format. In practice the font rendered fine with just the font-weight addition above, and MDN states that this isn’t strictly necessary. But what the hell, it’s a recommendation, so let’s add it.

If you want to start playing with variable fonts in your designs, Michelle's article will point you in the right direction.

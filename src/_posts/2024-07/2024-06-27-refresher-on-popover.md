---
date: Thu, 27 Jun 2024 14:55:53 -0700
title: A Good Refresher on Popovers
description: When you want your website to really pop(over), this is how you do it.
link_url: https://css-tricks.com/poppin-in/
---

Geoff Graham [writes over at CSS Tricks](https://css-tricks.com/poppin-in/):

> It’s only been since April of this year that we finally have full Popover API support in modern browsers.
> 
> There was once upon a time that we were going to get a brand-new `<popover>` element in HTML for this. Chromium was working on development as recently as September 2021 but reached a point where it was dropped in favor of a popover attribute instead. That seems to make the most sense given that any element can be a popover — we merely need to attach it to the attribute to enable it.

Lots of CodePen examples abound, as well as some nifty styling solutions.

The popover mechanism which eventually landed in browsers is way cool, and I'm itching to get a feature going where reaching for popover is the obvious call. Thankfully I'll now have Geoff's article as a primer to steer me in the right direction.

**Update:** Oh snap, [this example by Jhey Tompkins of a slide-out menu](https://codepen.io/jh3y/pen/VwGGqmm) using nothing but popover-based HTML & CSS is absolutely brilliant. 🤩

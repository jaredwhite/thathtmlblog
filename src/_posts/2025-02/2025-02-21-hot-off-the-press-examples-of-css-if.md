---
date: Fri, 21 Feb 2025 09:44:28 -0800
title: Hot Off the Press Examples of CSS if()
description: Taking context-aware theming to a whole new level…one day.
link_url: https://chriskirknielsen.com/blog/future-theme-with-inline-if-style-conditions/
---

Christopher Kirk-Nielsen is [back at it again](https://chriskirknielsen.com/blog/future-theme-with-inline-if-style-conditions/) showing us the power of futuristic CSS theming—this time via the experimental `if()` syntax (now available via a flag in Chrome Canary) rather than container style queries (which are not experimental but unfortunately also not yet available in Firefox).

From the article:

> Nearly two years later, I am _still_ waiting for style queries to be widely available, so I don't expect to be using this any time soon (though this approach includes fallbacks, so it should be safe, if used carefully).
> 
> Regardless, this is a wonderful addition to the CSS programming language which will let us do a lot of very, very cool things, especially with functions, and scopes, and layers, and… holy smokes, **CSS truly is awesome**, isn't it?

Yes, it most certainly is. 😊
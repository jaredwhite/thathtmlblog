---
date: Wed, 23 Aug 2023 09:54:36 -0700
title: Pros & Cons of Shadow DOM (with Lots of Examples)
description: A generally fair analysis of why you need shadow DOM and simultaneously which gotchas you might run into.
link_url: https://www.matuzo.at/blog/2023/pros-and-cons-of-shadow-dom/
---

I tend to cringe whenever I hear of another article "dealing" with shadow DOM, because for whatever reason it continues to feel like some freaky frothy topic in the world of web component architecture. But Manuel Matuzović has been a solid and respected author & speaker for some time in the world of web development—CSS & accessibility in particular—[so when he says something, I listen](https://www.matuzo.at/blog/2023/pros-and-cons-of-shadow-dom/).

Manuel writes:

> I summarized my pros and cons of Shadow DOM and style encapsulation. Please note that I'm writing this from the perspective of someone who, first and foremost, cares about accessibility and well-structured, semantic HTML. For me, JavaScript is an optional add-on and not the foundation of my code. That implies that I might not have to solve the same problems as others who primarily work with JS-heavy websites. That might also explain why my list of cons is longer than the list of pros.

I appreciate this, as I too deeply care about semantic HTML as well as progressive enhancement. And I'll admit, I *have* shied away from using shadow DOM on projects where I was principally working with server-rendered HTML and frameworks which aren't even JavaScript-based such as Ruby on Rails. This is fast-changing though with the arrival of Declarative Shadow DOM (DSD), which is easy enough to polyfill in a mere few lines of code in Firefox (the only remaining holdout) that I consider it ready for prime time today.

Manuel also writes:

> I understand that most of the cons described in this post are not critical issues, and there are ways to work around them.

Well said. And this is where so many of the conversations I tend to have with folks who are shadow DOM skeptics get tripped up. They'll present a very real list of cons much like Manuel has written up, and when I say "sure, but there are workarounds for some things and other things might not even be an issue on your particular project", somehow _that's bad_ and therefore _I'm bad_. 😅

Here's the deal: shadow DOM is part of the web platform. It is! It's not going anywhere. It's being used all across the industry for a wide variety of use cases. And it's _only_ going to get better, not worse, as the spec and the browser implementations improve. So it's _worth it_ for people to invest their time and resources into utilizing what it's good at and finding that sweet spot of best practices and workarounds which is what we have to do with _every_ technology. (I'm thinking I might write a tongue-in-cheek "Pros & Cons of Light DOM" article at some point because, believe me, it's not all peaches 'n' cream over there.)

At any rate, **I think this is a great article** and one I'll happily reference when debates come up in the future.

I'll leave with a [couple of comments](https://indieweb.social/@jaredwhite/110939931773703441) I posted on Mastodon:

> You did a fabulous job outlining a lot of the good and the bad of shadow DOM and really offering a “features list" for spec authors/browsers IMHO. Like, can’t we just fix all this stuff? That'd be pretty awesome! For me, the good still outweighs the bad and I'm "shadow DOM by default" at this stage in the game—even architecting frameworks and new best practices around it (and especially DSD).
>
> It's also worth mentioning that design system components which use shadow DOM can actually offer _improved_ accessibility. For example, the Shoelace component library puts a lot of effort into assigning all the appropriate ARIA roles and attributes (even reflecting live reactivity), and thus sometimes when I replace some custom markup I've hacked together with a Shoelace component, a11y on the page gets better.

(Seriously, [Shoelace rocks](https://shoelace.style).)

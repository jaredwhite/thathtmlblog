---
date: Fri, 29 Sep 2023 09:53:33 -0700
title: TypeScript Smackdown on the Bad At CSS Podcast
description: Adam Argyle absolutely eviscerates TypeScript and I'm so here for it.
link_url: https://zencastr.com/z/OODQwjy1
---

It's no secret I'm not a fan of TypeScript. Actually let me back up a second. I really do enjoy defining types for my classes, methods, and functions when writing a library—and _especially_ when I'm doing so using **what should be the default** for the TypeScript community which is to write vanilla JavaScript and put type information in comments using JSDoc. [This is the way.](https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html)

At any rate, the industry pivot to TypeScript has not sat well with me, yet after [listening to this outstanding podcast from Adam Argyle & David East](https://zencastr.com/z/OODQwjy1) where Adam absolutely _eviscerates_ TypeScript, I'm beginning to think I've mellowed out. 😂

There were lots of really, really good points made, so you should definitely take a listen. Even if you continue writing TypeScript proper, or better yet types-in-JS-with-JSDoc, I think you need to go into it clear-eyed and sober because [types can definitely become a crutch](https://www.spicyweb.dev/static-types-mask-code-smells/) and even an impediment to writing high-quality, readable, and accessible (as in you don't need a PhD in type systems to understand what the hell is going on!) code.

---
date: Wed, 26 Jul 2023 10:42:58 -0700
title: handleEvent() is Mindblowing
description: A feature I had no idea existed which works beautifully with web components.
---

Thanks to [Andrea Giammarchi](https://github.com/WebReflection) in a comment on a [pretty rad article about events in general](https://dev.to/thepassle/events-are-the-shit-b3i), I learned about a web feature I had no idea existed, and it is _blowing my mind_. I am of course talking about [`handleEvent()`](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener#specifying_this_using_bind). It shouldn't surprise us that it works beautifully with web components, and now I'm wondering why I ever wrote vanilla JS without it.

Here's a basic example:

```javascript
class MyComponent extends HTMLElement {
  static {
    customElements.define("my-component", this)
  }

  connectedCallback() {
    this.addEventListener("click", this)
  }

  handleEvent(event) {
    if (event.type === "click") {
      alert("You clicked me!")
    }
  }
}
```

Naturally I had to [write a Codepen](https://codepen.io/jaredcwhite/pen/QWJZpMM) to thoroughly test it out, and boy is it a _sweet solution_. I doubt I'll ever attach individual event handler methods within my components again.

**Addendum:** [Konnor Rogers](https://konnorrogers.com/) pointed out in [The Spicy Web Discord](https://discord.gg/CUuYVH7Qa9) that this provides the added benefit of avoiding clunky arrow function syntax in your class definition, or alternatively having to use `.bind()` when setting up event listeners, in order to access the proper `this`. Good stuff.

**Addendum #2:** If you're working with a shadow root inside your component, you'll likely want to attach the event listener to that rather than the element itself, e.g. `this.shadowRoot.addEventListener(...)`. This is because events within the shadow DOM are "composed" and therefore they get retargeted once caught by event listeners in light DOM. Which means you'd have no way of knowing which actual element in the shadow DOM triggered the event. But by attaching your listener to your shadow root, you can get at the proper `event.target`…plus it still works with any slotted content for your component as well. You can read [in-depth documentation about this here](https://javascript.info/shadow-dom-events).
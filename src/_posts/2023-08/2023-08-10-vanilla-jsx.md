---
date: Thu, 10 Aug 2023 12:17:57 -0700
title: “Vanilla” JSX if that’s your scene
description: While I prefer tagged template literals, Chris raises a good point.
link_url: https://chriscoyier.net/2023/08/07/jsx-without-react/
---

Chris Coyier [presents some examples of using JSX without React](https://chriscoyier.net/2023/08/07/jsx-without-react/) and talks about other flavors of JSX like Astro and even Preact SSR:

> I think it’s perfectly reasonable to want to use the JSX syntax but not need React. It’s a decent templating language that I bet a lot of people are quite comfortable with.

I still prefer tagged template literals in nearly all cases, but Chris is right that a lot of folks are familiar with JSX. If that familiarity can be used to our advantage in providing solutions which are better than React in a variety of cases, awesome sauce.

P.S. You can even write Vue components and build sites in Nuxt using JSX. Not that I would recommend it necessarily, [but it's true!](https://codesandbox.io/p/sandbox/github/nuxt/examples/tree/main/advanced/jsx?embed=1&file=%2Fapp.vue%3A1%2C1)
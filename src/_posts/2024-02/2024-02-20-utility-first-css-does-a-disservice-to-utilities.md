---
date: Tue, 20 Feb 2024 14:49:26 -0800
title: Utility-First CSS Does a Disservice to Utilities
description: When the C of CSS is simply being thrown away for the sake of a convenience that doesn't actually end up very convenient at all.
link_url: https://heydonworks.com/article/what-is-utility-first-css/
---

In [this excellent addition](https://heydonworks.com/article/what-is-utility-first-css/) to the corpus of critiques of "utility-first" CSS, Heydon Pickering writes:

> You can’t really appreciate utility-first CSS until you have a decent understanding of CSS itself, so this article will principally be about that. However, paradoxically, the more you learn about CSS, the less you may appreciate utility-first CSS. You might begin to question why it should exist at all. It’s possible you’ll even start to question why _you_ exist.
> 
> I’ll try my best to explain.

Heydon does an excellent job of explaining the strange world we find ourselves in where the **C** of **CSS** is simply being thrown away for the sake of a convenience that doesn't actually end up very convenient at all in the long run.

As [I've been saying](https://www.spicyweb.dev/css-nouveau/) for a [long time now](https://www.spicyweb.dev/the-three-laws-of-utility-classes/), I'm not against utilities in CSS per se, I'm against the _utility-first_ methodology. Heydon explains why starting with utilities, rather than ending up with them when and where they're truly useful, results in an architecture of styling HTML that really does a disservice to the platform…including the point of using utilities. How did we end up here? That's probably hard to answer in any one blog post, though Heydon touches on it. Let's just say well-funded marketing hype really does go a long way—though I'm optimistic we're already over the bell curve and utility-first is slowly on its way out. **Can't happen soon enough.**

---
date: Fri, 13 Oct 2023 08:35:53 -0700
link_url: https://css-irl.info/styling-external-links-with-attribute-selectors/
title: Add Icons to Your External Links Purely with CSS
description: You don't need to alter your markup in any way. Attribute selectors are your friend!
---

Apparently this is Michelle Barker day, as we have another excellent writeup from Michelle [this time all about how to style external links](https://css-irl.info/styling-external-links-with-attribute-selectors/) and add SVG icons without any additional HTML or JavaScript needed:

> I’ve just recently implemented custom styling for external links within posts on this site. The good news is we don’t need to append a special class to external links or alter the markup in any way. We can simply use an attribute selector.

👏 Attribute selectors are the bee's knees.

---
date: Fri, 08 Dec 2023 08:59:13 -0800
title: CSS Wrapped 2023 is So Long, I Didn't Read 😂
description: The amount of progress we've seen on the web platform in 2023 boggles the mind, and it'll keep right on going in 2024. Don't stop believin'!
link_url: https://developer.chrome.com/blog/css-wrapped-2023
---

The Chrome UI DevRel Team (Una Kravets, Bramus, and Adam Argyle) have [put together a lively rundown of all the goodies](https://developer.chrome.com/blog/css-wrapped-2023) we got in CSS this year…

…AND IT'S SO LONG I HAVEN'T EVEN GOTTEN THROUGH IT ALL YET. 🤯

Seriously, the amount of progress we've seen in 2023 is freaking insane, and 2024 is shaping up to be just as consequential. I don't know which deals were made with which deities to get us to this point, but if the web development gods are smiling down on us to this degree, all I can say is: _may their blessings flow!_

I think I'm most excited about all the [color functions](https://thathtml.blog/2023/08/using-color-functions-for-automated-tints/), [nesting](https://thathtml.blog/2023/11/is-2024-the-year-of-css-nesting/), and `:has` landing everywhere. The Popover API looks really intriguing as well (which is featured here even though it's more of an HTML thing).

What tickles your fancy?

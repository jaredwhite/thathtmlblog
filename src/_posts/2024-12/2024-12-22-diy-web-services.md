---
date: Sun, 22 Dec 2024 22:24:59 -0800
title: The Joyful Pleasures of DIY Web Services
description: "My advice to you is: avoid Docker like the plague!"
---

Sometimes **DIY** is the best way to go when it comes to setting up web services.

I'll try to write up the more nerdy specifics soon in a how-to, but in a nutshell I spent several hours today:

* Wanting to use [Codeberg](https://codeberg.org)\* for a code repo with a docs site built with [11ty](https://11ty.dev)
* My typical hosting provider of choice, [Render](https://render.com), doesn't support Codeberg 😢
* Thus trying to figure out how to set up a `Dockerfile`
* Trying to figure out how to deploy said file on Fly.io
* Casting about in a manic panic when every two steps forward I took one step back
* Eventually giving up because nothing makes sense anymore… 😭

I swear, I have _never_ had a great experience with deploying anything using Docker, and this is the _last_ time I make an attempt to engage with it. 😡

So I took a step back, and thought, and thought, and thought some more. And it occurred to me that I recently set up a VPS (droplet) on [Digital Ocean](https://digitalocean.com), and I could easily use it to serve the static files for the 11ty site. I already had [Caddy](https://caddyserver.com) set up there which is the coolest little web server in the world. Easy peasy!

The big question then became: **how do I build and deploy the site any time I push a commit up to Codeberg?**

And the answer which I now have working essentially boils down to:

* I set up a [webhook](https://forgejo.org/docs/latest/user/webhooks/) in the repo on Codeberg
* I wrote a little [Roda server](https://roda.jeremyevans.net) to process the incoming webhooks, and set it up as a systemd service
* When the hook POST arrives, it runs a series of commands to go into the docs site folder, do `git pull`, install NPM packages, and run the build command
* Those static files are immediately servable by Caddy. Site updated and available to the public!

I would have attempted this a lot sooner, except that dealing with incoming webhooks and managing the Git workflow is something that in the past I found a real pain to figure out. But I'm glad I pushed through this time, and now that I have this working, **I'll be setting up a whole bunch of static sites this way going forward**.

\* If you're wondering _why Codeberg?_, the answer is I'm not touching GitHub Copilot with a ten foot pole, and now that they're forcing it down our throats right on the homepage, it's time for me to migrate as much as I can to a Git forge which is truly open source and not beholden to Micro$oft's corporate interests.
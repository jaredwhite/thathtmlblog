---
date: Mon, 24 Jul 2023 08:50:02 -0700
title: Shining Light on the Shadow DOM (Video)
description: An in-depth presentation by Cassondra Roberts at CSS Day 2023.
link_url: https://www.youtube.com/watch?v=67bSCEEdaH8&list=PLjnstNlepBvOG299LOrvMFJ8WreCDWWd4&index=14
---

I'm just starting to watch through all the videos from CSS Day held in June in Amsterdam. [This talk by Cassondra Roberts](https://www.youtube.com/watch?v=67bSCEEdaH8&list=PLjnstNlepBvOG299LOrvMFJ8WreCDWWd4&index=14) provides an excellent look at building web components using shadow DOM, design systems, and the latest browsers APIs.

> Together we’ll examine the cascade and learn how inheritance works when Shadow DOMs get involved. After a crash-course in terminology, we’ll explore some of the “gotchas”, look at browser inconsistencies, and I’ll proffer some compatibility approaches that won’t give you a migraine. We’ll wrap things up by zooming out and examining how we can use Web Components to build consistency in a codified design library through theming.

[Watch on YouTube](https://www.youtube.com/watch?v=67bSCEEdaH8&list=PLjnstNlepBvOG299LOrvMFJ8WreCDWWd4&index=14).
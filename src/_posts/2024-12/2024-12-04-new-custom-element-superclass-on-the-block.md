---
date: Wed, 04 Dec 2024 09:11:30 -0800
title: There’s a New Custom Element Superclass on the Block
description: "Reactive Mastro: a tiny reactive GUI library for your existing MPA."
link_url: https://github.com/mb21/mastro/tree/main/src/reactive
---

It's always a good day when I learn about a new library to aid in **authoring web components**. And this time around, I got a tip in my inbox (<a href="mailto:<%= site.metadata.email %>"><%= site.metadata.email %></a>) all about a new kid on the block, [Reactive Mastro](https://github.com/mb21/mastro/tree/main/src/reactive).

Mastro itself is a MPA-oriented JavaScript web framework currently in the works by [Mauro Bieg](https://github.com/mb21), and the client-side reactivity part of it is what we're looking at here. It's very small (2.6kB), especially when considering that includes the signals library which Reactive Mastro is dependent on, [@maverick-js/signals](https://github.com/maverick-js/signals), and it provides a clear path to writing custom elements which are primarily driven by server-rendered HTML but need some additional interactivity after the fact.

Here's a typical counter example provided by Reactive Mastro:

```html
<my-counter>
  Count is <span data-bind="count">0</span>
  <button data-onclick="inc">+</button>
</my-counter>
```

```js
import { ReactiveElement, signal } from "mastro/reactive"

customElements.define("my-counter", class extends ReactiveElement {
  count = signal(0)

  inc () {
    this.count.set(c => c + 1)
  }
})
```

I created a larger [CodePen demo](https://codepen.io/jaredcwhite/pen/GgKpbva) to show some of these features in action: wiring up event handlers, binding DOM elements to signal values, reacting to signal mutations in custom effects, etc. The syntax is easy to learn and rather elegant, and it reminds me of my past days working with HTML containing bindings to Knockout or Vue models.

The one knock I have against Reactive Mastro: no support currently for shadow DOM, only content in light DOM. [I filed an issue here](https://github.com/mb21/mastro/issues/2) if you're interested in tracking it.

Nevertheless, I think this is a library worth checking out! It might be the fastest route to experimenting with signal-based reactivity. As the author puts it:

> Reactive Mastro sits somewhere in between React/Vue/Solid/Svelte one one end, and Alpine/HTMX/Stimulus on the other end – while being smaller and simpler than all of them. It’s a thin wrapper around “HTML Web Components” and signals, and it can be used with any static site or server returning HTML. 

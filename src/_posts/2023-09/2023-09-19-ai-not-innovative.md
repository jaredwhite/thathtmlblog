---
date: Tue, 19 Sep 2023 11:46:02 -0700
title: AI-generated UIs Just Not That Innovative
description: Think you'll get “production-grade” websites with the click of a button? Think again.
link_url: https://heather-buchel.com/blog/2023/09/ai-generated-ui-is-not-innovative/
---

I think we're fast coming up to the peek and inevitable bust of the "generative AI" hype cycle, but in the meantime, we still must suffer from the hyperbole and the very real harm these tools (and often their champions) can cause.

[Heather Buchel lays it all out in her excellent essay](https://heather-buchel.com/blog/2023/09/ai-generated-ui-is-not-innovative/) related to the recent unveiling of "v0.dev" by Vercel:

> Not everyone is claiming that their AI tool is particularly innovative. But like most things involving AI right now, we are seeing some pretty wild claims.
> 
> This is where we are and it's where we've been before. We've been in this same scenario with design tools prior to this current AI craze; tools that claimed designers could build responsive, accessible websites straight from the design or WYSIWYG editor. And maybe we got a little close to that, as long as your website was mostly text, images, and links to other pages that were just text and images. Even in those "close" scenarios, you'd probably still be left with out of order headings and unlabelled navigation elements.
> 
> And, your developer would wonder how on earth are they going to keep design-generated code maintained and on par with what they actually end up shipping in production.
> 
> So here we are again.

Every few years, this idea that website production, design work, and content development can all just be "automated" for the most part rears its ugly head. What's particularly grating is when we see these sorts of claims come from vendors who ostensibly are there to _help_ web developers get their job done.

It's like when Uber kept trumpeting about their program to develop self-driving taxis—_when the literal function of Uber is to help HUMANS find work driving people around_. Way to go throwing your own workforce under the bus (a self-driving bus, presumably).

Anyway, I really appreciated this read, and the main **TL;DR** takeaway is: if your "production-grade" generated code isn't fully accessible…well, _it ain't production-grade_. Full stop.

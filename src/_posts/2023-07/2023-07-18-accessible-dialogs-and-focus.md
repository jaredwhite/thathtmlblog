---
date: Tue, 18 Jul 2023 07:39:40 -0700
title: Accessible Dialogs and Focus
description: When you click a button and call the `showModal()` method to open a modal `<dialog>`, where does the focus go by default, and how can you move it elsewhere?
link_url: https://www.matuzo.at/blog/2023/focus-dialog/
---

Manuel Matuzović writes in “[O dialog focus, where art thou?](https://www.matuzo.at/blog/2023/focus-dialog/)”:

> Here’s a job interview question for you: When you click a button and call the `showModal()` method to open a modal `<dialog>`, where does the focus go by default, and how can you move it elsewhere?
> 
> Can you imagine a job interviewer caring about accessibility and HTML? Me neither, but the question is interesting nevertheless. I didn't know the answer, so I tested it.

As luck would have it, I was testing out `<dialog>` for a new component right when this article dropped, so Manuel's findings came in very handy. I also discovered I needed to use ARIA labeling on the `<dialog>` element itself for VoiceOver to announce it properly. Details, details! (But not `<details>`, that's for another time…)

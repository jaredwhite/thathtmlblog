---
date: Tue, 10 Oct 2023 14:46:37 -0700
title: The Organizing Principle of Web Components
description: Reason about your HTML, CSS, and JavaScript in modular units, rather than merely as scattered code applied to a page somehow.
link_url: https://blog.jim-nielsen.com/2023/web-components-icon-galleries/
---

Jim Nielsen recently redesigned his websites which showcase various icons designed for iOS, macOS, and watch OS. (Great job!) In the [writeup of how he built these updated sites](https://blog.jim-nielsen.com/2023/web-components-icon-galleries/), he talks about his solution making use of web component APIs:

> This is a nice approach (over stringing together a bunch of functions in JS file) because it feels more encapsulated. All related logic and DOM manipulations are “under one roof”. Plus it works as a progressive enhancement.
> 
> Maybe I shouldn’t be using the term “web component” for what I’ve done here. I’m not using shadow DOM. I’m not using the templates or slots. I’m really only using custom elements to attach functionality to a specific kind of component.
> 
> But it still kinda feels like web components. All of this could’ve been accomplished with regular ole’ web techniques…but custom elements give me an organizing principle for my code that allow me to more easily understand and intuit the scope of styles and scripts.

I love that term **organizing principle**. Custom Elements gives you a browser-approved organizing principle, letting you reason about your HTML, CSS, and JavaScript in modular units, rather than merely as scattered code applied to a page somehow.

It's also interesting to contemplate Jim's (and Zach Leatherman who's referenced here) question about if we need a name for this sort of component…aka one which mainly exists just as a way to hang some CSS and JS off a part of a page, and not something that's officially part of some "design system" or hardcore "application UI" or whatnot.

Hmm, well I don't know…I've always just been building web components like this. 🤷🏻‍♂️ For years now. For example, I've converted many a [Stimulus](https://stimulus.hotwired.dev/) controller over to a web component because—while I like Stimulus—I also like not needing yet another dependency with its own bespoke API. Pretty much any UI logic I ever write now I wrap up inside of a custom element one way or another.

So do we need a name for this? I could offer up a suggestion: **Webby Web Components**. Or perhaps **WondrousAmazeballsSensationalMagnificent (WASM) Components**. Dang, that acronym's already been taken!

_What do you think?_ 😜

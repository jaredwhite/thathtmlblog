---
date: Wednesday, May 8, 2024 at 8:53:22 AM PDT
title: "Props v Attributes: Go!"
description: For folks mainly used to operating under abstracted frontend frameworks, this article may prove most enlightening.
link_url: https://jakearchibald.com/2024/attributes-vs-properties/
---

It's easy to get confused about how to work with element properties in JavaScript vs. how to work with element attributes in HTML. [Jake Archibald to the rescue](https://jakearchibald.com/2024/attributes-vs-properties/):

> Most of the time, these distinctions don't matter. I think it's good that developers can have a long and happy career without caring about the differences between properties and attributes. But, if you need to dig down into the DOM at a lower level, it helps to know. Even if you feel you know the difference, maybe I'll touch on a couple of details you hadn't considered.

In this article, Jake talks about value types, serialization, reflection, case sensitivity, and a host of other interesting details. Some of this may be old hat if you're used to working with vanilla JS, but for folks who are mainly used to operating under abstracted frontend frameworks, it may prove most enlightening. (And if you need to write/employ web components, this knowledge is a must!)
---
date: Wed, 17 Jan 2024 15:11:05 -0800
title: What Prelude-less Scope Is and Why You Want It
description: Essentially what you imagine when you think of defining "scoped styles" in your HTML.
link_url: https://developer.chrome.com/docs/css-ui/at-scope#prelude-less_scope
---

Now available in Safari Technology Preview 186+ as well as Chromium-based browsers from 118 onward, `@scope` holds the potential to reshape how we think about formulating CSS selectors and handling scoping considerations.

But my favorite feature is what is called _prelude-less scope_, and it is directly tied to the use of `<style>` tags inline in HTML. It's essentially what you imagine when you think of defining "scoped styles" in your HTML. Here is a very simple example:

```html
<section>
  <h1>Hello World.</h1>
  <style>
    @scope {
      h1 { color: red }
    }
  </style>
</section>

<section>
  <h1>Not Red!</h1>
</section>
```

In supported browsers, this will color the `h1` element within the scope of the first `section` element, but not within any other `section` element. If you wanted to "scope" things even further, you could write `> h1` and then only that element directly inside the section would be affected, not any additionally-nested elements within the child hierarchy.

This promises to be a fantastic way of quickly rustling up some styles (particularly overrides) of any one template or component without having to worry much about the typical scoping-type solutions (special classes/ids/attributes etc.) folks reach for. Heck, even if you just wish to set a bunch of CSS variables in a particular part of the DOM tree, that would be useful. Or imagine pulling some inline styles out of a `style=` attribute and using `<style>` instead for much the same purpose, only then you don't suffer any of the limitations which come with using style attributes (hello media/container queries)!

[Here's a Codepen with another example of what you can accomplish.](https://codepen.io/jaredcwhite/pen/BaMxdxJ)

[More details around `@scope` and how to use it is available at the Chrome Developer blog.](https://developer.chrome.com/docs/css-ui/at-scope#prelude-less_scope)
---
date: Fri, 13 Oct 2023 08:25:17 -0700
title: A More Sustainable Web
description: Those of us in the web industry must do our part to combat climate change.
link_url: https://developer.mozilla.org/en-US/blog/introduction-to-web-sustainability/
---

So it turns out building more lightweight web applications (on both the backend and the frontend) is not only great for both developers and users, but _also the planet_ as [Michelle Barker makes clear writing for MDN](https://developer.mozilla.org/en-US/blog/introduction-to-web-sustainability/):

> As its impacts become more evident and frequent, climate change is quickly moving up the agenda for governments, communities, and companies worldwide. We know that reducing our greenhouse gas emissions is crucial to mitigate the worst effects of climate change and ensure a liveable and sustainable future.
> 
> Every industry has a part to play, including those of us in the web industry. In this article, we’ll explore some of the environmental impacts of our digital lifestyles and discuss how web designers and developers can contribute to more sustainable practices.

Perhaps my favorite quote from the article: "Closely consider whether you really need a JavaScript framework for your project." 😁

---
date: Tuesday, April 25, 2024 at 4:29:57 PM PDT
title: The Future of Masonry Layout in CSS
description: Help shape the future of this important web layout specification.
link_url: https://webkit.org/blog/15269/help-us-invent-masonry-layouts-for-css-grid-level-3/
---

More than just the "Pinterest layout", I feel like we've been waiting for a JavaScript-free solution to content flowing down multiple columns for _forever_. But the momentum to add a "masonry grid" to CSS proper is growing, and this layout mode is being prototyped as we speak. [Over at the WebKit Blog, Jen Simmons writes:](https://webkit.org/blog/15269/help-us-invent-masonry-layouts-for-css-grid-level-3/)

> What do we mean by the term “masonry layout”? Basically it’s the pattern seen in the following image — where content packs together like a brick or stone wall. That’s where it gets the name “masonry”. It’s also frequently called “waterfall layout”, as a metaphor for how content flows down the page like a waterfall.
> 
> This layout is popular because it solves a few problems that other layouts do not.
> 1. It allows for content of different aspect ratios, and avoids the need to crop or truncate content in order to turn everything into uniform rectangles.
> 2. It distributes content across the page (instead of flowing down each column, one by one). This follows the natural reading order as you scroll the page. And it lets the website lazy-load additional content at the bottom without moving existing content around.

![Masonry example](/images/image1-masonry-light.png){: loading="lazy"}

But as with any complex spec like this, questions abound just how this should work exactly alongside existing layout modes. Is masonry something that should be part of CSS Grid per se? And should it even be called "masonry" in the first place? (That's a very colloquial English-language term not easily translated to all other languages and cultures.)

Jen Simmons does a great job, as always, laying out many of the ideas, questions, and ultimately requests for feedback involved in this spec. I recommend you [give the article a thorough read](https://webkit.org/blog/15269/help-us-invent-masonry-layouts-for-css-grid-level-3/) and consider your perspective as this important spec gets shaped before joining the web platform.
---
date: Fri, 03 May 2024 10:34:25 -0700
title: A Different Take on Masonry from the Chrome Team
description: How will the different browser vendors square this circle?
link_url: https://developer.chrome.com/blog/masonry
---

_Oh, the drama!_

Rachel Andrew [writing on behalf of the Chrome Team](https://developer.chrome.com/blog/masonry):

> The Chrome team is keen to see an implementation of masonry type layouts on the web. However, we feel that implementing it as part of the CSS Grid specification [as proposed in the recent WebKit post](https://thathtml.blog/2024/04/the-future-of-masonry-layout/) would be a mistake. We also feel that the WebKit post argued against a version of masonry that no one was proposing.
> 
> Therefore, this post aims to explain why we at Chrome have concerns about implementing masonry as part of the CSS Grid Layout specification, and clarify exactly what the alternate proposal enables.

Rachel does a fine job of explaining how the "masonry" layout differs enough from standard CSS grid so as to warrant a different layout mode—even though many concepts can be shared between the two features. Performance concerns are top of mind, which is very understandable.

Jeremy Keith [isn't buying it though](https://adactio.com/links/21088):

> I’m not entirely convinced. We heard performance issues as a reason why we could never have container queries or :has, but here we are. And the syntax for a separate masonry spec borrows so heavily from grid that it smells of reduncany [sic].

**What do you think?** Should masonry be an addition to CSS grid? Or should it be its own layout mode? Share your thoughts over at **The Spicy Web** [Mastodon](https://intuitivefuture.com/@vanillaweb) or [Discord](https://discord.gg/CUuYVH7Qa9)!

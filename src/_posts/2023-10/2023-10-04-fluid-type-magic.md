---
date: Wed, 04 Oct 2023 12:20:32 -0700
link_url: https://utopia.fyi/clamp/calculator/
title: Fluid Type Variable Magic
description: I have absolutely no idea how it works. But I do understand the reason for it to work.
---

In the words of [Councillor Hamann](https://warnerbros.fandom.com/wiki/Councillor_Hamann): "There is so much in this world that I do not understand. See that (Utopia clamp calculator)? I have absolutely no idea how it works. But I do understand the reason for it to work."

Fluid type is all the rage now on the web, thanks to CSS' `clamp` function. However, figuring out just what to _do_ with `clamp` can be a challenge in and of itself.

Thankfully, the folks at Utopia have provided us with a [clamp calculator](https://utopia.fyi/clamp/calculator/) for generating fluid type variables (or any sort of spacing really) based on a simple set of rules. I still don't really grok all the calculations they're doing within each function, but the fact you can set up your viewport and size ranges and get it to "figure it all out" for you is pretty rad.

There's also a dedicated [fluid space calculator](https://utopia.fyi/space/calculator) which is equally spiffy, and much more besides. I definitely recommend keeping these in your bookmarks!
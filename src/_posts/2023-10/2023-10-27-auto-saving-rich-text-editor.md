---
date: Fri, 27 Oct 2023 00:05:33 -0700
title: Auto-Saving Rich Text with RhinoEditor
description: It looks slick and it's delivered as a web component—which means you can drop it right in pretty much anywhere.
link_url: https://codepen.io/jaredcwhite/pen/vYbNVMe
---

I feel like at various times over the years, all I've wanted is an attractive, drop-in rich text editor which can also auto-save by posting the form it's in. Easy to use, easy to style, _easy to customize_.

Well, Konnor Rogers (who never ceases to amaze me) to the rescue! His new [RhinoEditor](https://rhino-editor.vercel.app/) component was built to be a replacement for Trix, a popular editor from Basecamp which may be easy to use for the public but is not so easy to customize and work with from the developer side. While it offers support for Rails' ActionText framework, you can actually use it apart from Rails easily as long as you don't need file attachments (which I didn't for the Notes editor I just launched over at [The Spicy Web Courses](https://www.spicyweb.dev/courses)).

Here's a [CodePen](https://codepen.io/jaredcwhite/pen/vYbNVMe) demonstrating how you can load it up, wrap it in a form, tweak some styles along with disabling file attachments, and generally get it working with minimal fuss.

<iframe height="400" style="width: 100%; display: block; margin-block: var(--size-7);" scrolling="no" title="Auto-saving Rich Text with RhinoEditor" src="https://codepen.io/jaredcwhite/embed/vYbNVMe?default-tab=html%2Cresult" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true"></iframe>

 I'm quite excited about [RhinoEditor](https://rhino-editor.vercel.app/) (which itself is built on top of the solid foundation of ProseMirror / TipTap) because it looks slick _and_ it's delivered as a web component—which means you can drop it right in pretty much anywhere. **Groovy.** 😎
---
date: Sun, 16 Jul 2023 22:05:30 -0700
title: "Just a Spec Podcast: Obscure (but Awesome) HTML Tags"
description: Learn about some of our favorites young and old which may pique your curiosity.
link_url: https://justaspec.buzzsprout.com/1863126/13166260-our-top-obscure-but-awesome-html-tags
---

Of course, with the launch of this new blog, I must indulge in a wee bit of self-promotion. 😏 Ayush and I had a lot of fun with this episode, talking all about some of our favorite HTML tags which you may or may not have heard of…

> Web developers and content authors have a rich array of tags to chose from as they build experiences using HTML. Learn about some of our favorites young and old which may pique your curiosity, as well as enjoy a spirited refresher on the importance of writing semantic and accessible HTML.

[I hope you enjoy the listen!](https://justaspec.buzzsprout.com/1863126/13166260-our-top-obscure-but-awesome-html-tags) (Next episode's coming out shortly, so please subscribe in your podcatcher of choice.)

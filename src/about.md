---
layout: page
title: About the Blog
---

Well hello there! I'm [Jared White](https://jaredwhite.com), and I'm a web developer. Started in 1996, and…yep, still here. 😆

I'm also a blogger. Have been for as long as I've been a web developer. To me, developing websites and writing for websites go together as naturally as peanut butter & jelly, salt & pepper, enchiladas & pickles…hey, don't knock it ’till you try it. 🌶️🥒

<img
  src="https://jaredwhite.com/images/jaredwhite-in-bw.jpg"
  alt="Black & white portrait of Jared"
  style="
    float: right;
    width: clamp(150px, 25vw, 300px);
    border-radius: 3px;
    margin-inline-start: clamp(var(--size-4), 5vw, var(--size-6));
    margin-block-end: var(--size-4);
    box-shadow: 4px 10px 30px -15px rgba(75,15,0,0.85);"
/>But then a funny thing happened. **Big Social** came along. And Big Social convinced us we don't need blogs. Or even if you simply _must_ have a blog, you only need to link to it from social media. _Curated aggregators? Pfff, who needs 'em!_

Well, it turns out—now that we're fully ensconced in the 21st century—Big Social sucks monkey balls, and **we desperately need curated aggregators** in the "blogosphere" once again. It's great that bloggers have started to embrace the indie web once again, and _RSS is back, baby!_ (Don't call it a comeback.) Yet discovering and then following dozens and dozens of RSS feeds to stay on top of what's really new and cool in the world of **#WebDev** is a lot of work.

**So let me do the work for you!**

Each day (or thereabouts), I scour the interwebs, looking for fresh content to, uh, aggregate here on **That HTML Blog**. I use my vast powers of deduction to separate the wheat from the chaff, the grapes from the honeydew melons, the chocolate bars from the sour candies, the…well, you get the idea. 🍫

## Why "Vanilla"? Why "Standards-Adjacent"? Where My Next.js / TypeScript / React / Tailwind Links At?

One of the reasons we _desperately_ need curated blog aggregators again in the web development space is our industry has been overrun by Big Tech, VC-backed corporations, and celebrity influencers who have been shockingly effective at convincing a generation of web developers that "you can't build real apps using vanilla web technologies." You need their solutions (of course). The FUD is real.

The irony here is the immense power and capability you can get in every modern web browser is **astounding**. I would have sold my left kidney 10 years ago to use a web browser as good as the one in your pocket _right now_. And yet…somehow, we're led to believe these web browsers suck on their own, and you need the features of [insert trendy framework name here] to get them to do anything useful.

Poppycock! Balderdash! Hogswallop! At **That HTML Blog**, we have a simple philosophy: **YAGNI**. Until you really, truly, absolutely need [insert trendy framework/build tool/cloud platform name here] because literally no other solution available on planet earth will help you get your project out the door—_and you can articulate why in verifiable technical detail_—Ya Ain't Gonna Need It.

So this blog is all about the basics—which are anything but basic. HTML. CSS. JavaScript. The latest work done by real standards bodies like the W3C. The latest features available in modern browsers. The latest innovations in networking and internet protocols. And how you can leverage all of this raw power to build lean, mean, fightin' website machines. 💪

And we also cover "standards-adjacent" and standards-aligned technologies, meaning frontend libraries and fullstack web frameworks which steer you _towards_, not away from, vanilla tech. The goal of most web tooling should be: you can dump it if the platform gains this feature. **Avoid vendor lock-in at all cost.** That [insert trendy framework/build tool/cloud platform name here] new hotness you think "everybody's using" today? It could be old and busted tomorrow. Are you merely learning tooling du jour, over and over again? Or are you learning the "native" web platform, a platform which has been relevant for 30 years and counting—and will _still_ be relevant 30 years from now?

## Why Do I (Jared) Care So Much?

I got into this whole web thing originally because I saw it as this incredible equalizer and force for individual creative expression. I guess you could say I'm an "internet hippie" and I always will be. So nothing bugs me more than companies which come along and try to convince the n00bs the only way they can successfully build for the web is to do it their (proprietary) way.

I firmly believe in a "polyglot" web, where a variety of languages, tools, operating systems, and philosophies can all build upon and innovate on top of the core web platform. I believe in open specifications and open protocols, not vendor lock-in. The podcast I host with my friend Ayush, [Just a Spec](https://justaspec.show), is literally about just that! And the stuff I write and produce over at [The Spicy Web](https://www.spicyweb.dev) is along similar lines. 🌶️

## I Am Blogger (and So Can You!) \*

The exciting thing is, I'm hearing from more and more people they're exhausted with the constant hype cycle too and just want to learn how to build for the native web platform, free from unnecessary complexity and cognitive overload. We all know this demand is out there, but with the implosion of Big Social and the "in between" time we find ourselves in across the blogosphere, bringing that sense of independent community back again is a challenge.

So let's do this thing. _Join the revolution!_ 🎆 **That HTML Blog** is principally an aggregator,  so it's only as good as the content _you_ write or recommend. [Let us know what we should feature](/contact) here on the site, and what you'd like to see more of! 👀

\* <small>[reference](https://en.wikipedia.org/wiki/I_Am_America_(And_So_Can_You!))</small>

----

## Technical Bits

This site is built using [Bridgetown](https://www.bridgetownrb.com), a Ruby-powered site generator I help maintain, and is hosted on [Render](https://render.com).

The logo is based off a font by StereoType called [Beyond the Mountains](https://www.stereo-type.fr/fonts/beyond-the-mountains/). The heading font is [Tiempos Fine](https://klim.co.nz/retail-fonts/tiempos-fine/) by Kilm Font Foundry, and the body font is [Metropolis](https://www.1001fonts.com/metropolis-font.html) by Chris Simpson.

The icon set is provided by [Remix Icon](https://remixicon.com).

The theme is hand-crafted by me using vanilla CSS, and it uses [Declarative Shadow DOM](https://konnorrogers.com/posts/2023/what-is-declarative-shadow-dom/) to separate presentational layout markup from the content markup as well as deploy CSS Grid.

I'm still tinkering with a bunch of stuff, but I hope to open up a public repo for the site shortly. And longer term, turn this layout into a generic Bridgetown blog theme. _Stay tuned!_

---
date: Mon, 08 Jul 2024 08:52:47 -0700
title: I Am Once Again Asking You to Build Progressively Enhanced Websites
description: A methodology for building high-quality web applications with a robust architecture which will stand the test of time.
link_url: https://piccalil.li/blog/its-about-time-i-tried-to-explain-what-progressive-enhancement-actually-is/
---

![Bernie Sanders is once again asking you to build progressively enhanced websites](/images/bernie-like-progressive-enhancement.jpg)
{:style="text-align:center"}

Listen to Bernie! 😊

But also, listen to [Andy Bell on the Piccalilli blog](https://piccalil.li/blog/its-about-time-i-tried-to-explain-what-progressive-enhancement-actually-is/) explain what progressive enhancement actually is:

> Progressive enhancement is a design and development principle where we build in layers which automatically turn themselves on based on the browser’s capabilities. Enhancement layers are treated as off by default, resulting in a solid baseline experience that is designed to work for everyone.
>
> We do this with a declarative approach which is already baked in to how the browser deals with HTML and CSS. For JavaScript — which is imperative — we only use it as an experience enhancer, rather than a requirement, which means only loading when the core elements of the page — HTML and CSS — are already providing a great user experience.

Andy goes on to stress the fact that this isn't "just an anti-JavaScript thing, it’s a mental model rooted in iteration" and that's exactly the approach I often recommend as well.

Web development isn't HTML _and_ CSS _and_ JavaScript. It's HTML, _then_ CSS, _then_ JavaScript. Get your semantics and your content right first. Then get your presentation right. Then get your client/server interactions right. Then integrate JavaScript behavior when and where it makes sense.

I'll admit, I'm more tolerant of a "middling" baseline than some. I don't think it's a problem if the no-JS version of a form is much simpler than the JS version for example. The real issue is when no-JS means your entire experience simply breaks and **the user is stuck**. You _never_ want your user to encounter a dead-end or a blank screen. That goes again the grain of the web, as they say.

HTML, _then_ CSS, _then_ JavaScript—that's not just a recipe for UX success, it's a methodology for building high-quality web applications with a robust architecture which will stand the test of time.

---
date: Friday, August 16, 2024 at 9:44:49 AM PDT
title: Action Web Components Which Span the Server-Client Divide
description: HTML-first web development is all the rage with a growing cadre of application authors. Here's an exciting path you can take to a dependency-free, buildless-compatible architecture.
link_url: https://www.spicyweb.dev/action-web-components/
---

Over at **The Spicy Web**, [I published an article about an HTML-first, full-stack UI pattern](https://www.spicyweb.dev/action-web-components/) I've grown to love, with roots spanning all the way back to the Ajax revolution of the 2000s:

> Similar to how an HTML Web Component in popular parlance is a custom element which wraps standard HTML and applies interactivity to those child elements when rendered on a page, an Action Web Component performs a one-time operation when it’s rendered on the page and then (not always but typically) removes itself from the DOM upon completion. These actions don’t themselves have any visual appearance—they aren’t content, but commands.
> 
> Action Web Components offers an intriguing paradigm which is compatible with a near-infinite number of client/server topologies. It’s totally framework-independent, and save for a wee bit of client-side JavaScript, it’s totally language-independent as well.

It features a demo repo built on top of **Astro** and **Alpine.js** (and **Pico CSS**)…but as the examples show, you can use literally any backend or frontend technology. The techinique itself is strictly vanilla, and for folks already familar with libraries like **Hotwired Turbo**, you will feel right at home.

[Check out the demo!](https://action-web-components.fly.dev/)
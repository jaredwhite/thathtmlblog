---
date: Fri, 29 Sep 2023 09:26:26 -0700
title: "Just a Spec Podcast: Fast Frontend Testing, Storage, Observers, Fetch, & More"
description: "A whole grab bag of topics…and apparently, fetch DID happen!"
link_url: https://justaspec.buzzsprout.com/1863126/13680822-fast-frontend-testing-storage-observers-fetch-more
---

Ayush and I [covered a lot of ground](https://justaspec.buzzsprout.com/1863126/13680822-fast-frontend-testing-storage-observers-fetch-more) in the latest installment of Just a Spec:

> A whole grab bag of topics today! We talk about some of the well-known storage APIs like localStorage and sessionStorage, as well as the newer IndexedDB API which pairs well with Service Workers. We also talk about the three Observer APIs to help with reacting to DOM mutations, scrolling, and size changes. Apparently fetch DID happen (!), and testing against three headless browsers at once in a fast and reliable manner is easier than ever. All that and more in today’s episode of Just a Spec.

Also ICYMI, we've had a string of action-packed episodes from [one all about accessibility](https://justaspec.buzzsprout.com/1863126/13417685-the-accessibility-tree) to one about [HTML over the wire](https://justaspec.buzzsprout.com/1863126/13498828-turbo-astro-server-components-and-html-over-the-wire) and "Turbo" architecture to a recent one about [the State of CSS Survey results](https://justaspec.buzzsprout.com/1863126/13585566-a-state-of-css). If you like listening to podcasts and you like web development, this is the show for you! ☺️

---
date: Thu, 07 Mar 2024 14:51:17 -0800
title: Rejiggering the Jamstack for Sustainability
description: Reports of its death have been greatly exaggerated?
---

Back in mid-2023, I wrote [My Journey Away from the JAMstack](https://www.spicyweb.dev/farewell-jamstack/) over at **The Spicy Web**. I was feeling, well, _spicy_ about how the marketing term "Jamstack" (née JAMstack) had been handled by its originator, Netlify. Based on a increasing fuzziness—dare I say _widespread confusion_—about what the term even meant, coupled with seemingly an abandonment by Netlify in favor of talking about the "composable web" for enterprises, I assumed we'd reached the end of the line for this particular concept in web developer parlance.

Well, I'm happy to report it looks like rumors of the death of Jamstack _was greatly exaggerated!_ A number of folks in this space, and one company in particular—**CloudCannon**—have spoken out in favor of rebooting the marketing and the community vibe around Jamstack and getting it back to its roots so to speak: aka a methodology around building websites which are portable, immutable, and deliver HTML/CSS/JavaScript assets cheaply and efficiently. In other words, **static sites**. 😍

I've appeared on CloudCannon's video channels twice now ([here](https://www.youtube.com/watch?v=HqbeSsd6-j4) and [here](https://www.youtube.com/watch?v=ehl15mDan1w)) as well as on a panel moderated by Zach Leatherman ([here](https://thathtml.blog/2023/09/jamstack-zhuzh/)) to talk about Jamstack, static sites, and web standards, and I think it's pretty awesome to see this lively conversation continuing. I recommend subscribing to CloudCannon's [Future of the Jamstack](https://www.youtube.com/channel/UCuEEpn3rI7mMYVxrfS9b8_w) channel to follow the progress of rejiggering "Jamstack" so that it's sustainable, community-owned, and noteworthy for what it _doesn't_ represent as much as what it does—because as we all know, a term that describes everything describes nothing.

Aside: I was asked if the static site generator I work on, [Bridgetown](https://www.bridgetownrb.com), might do a 180 and start advertising itself as a "Jamstack framework" again. I'm not _quite_ ready to declare victory, but based on these discussions about the future of Jamstack, we're getting real close! ☺️